#namespace("crm.receivablesplan")
  #sql("queryByContractId")
    SELECT * FROM 72crm_crm_receivables_plan where contract_id = ? order by num desc limit 0,1
   #end
   #sql("queryListByContractId")
     select scrp.plan_id,  scrp.num,scc.customer_name ,scco.num as contract_num ,scrp.remind,
     scrp.money,scrp.return_date,return_type,
     scrp.remind,scrp.remark
                     from 72crm_crm_receivables_plan as scrp
                    LEFT JOIN 72crm_crm_customer as scc on scc.customer_id = scrp.customer_id
                     LEFT JOIN 72crm_crm_contract as scco on scco.contract_id = scrp.contract_id
                    where scrp.contract_id = ?
   #end
    #sql("queryListByCustomerId")
    select scrp.plan_id,  scrp.num,scc.customer_name ,scco.num as contract_num ,scrp.remind,
     scrp.money,scrp.return_date,return_type,
     scrp.remind,scrp.remark
                     from 72crm_crm_receivables_plan as scrp
                    LEFT JOIN 72crm_crm_customer as scc on scc.customer_id = scrp.customer_id
                     LEFT JOIN 72crm_crm_contract as scco on scco.contract_id = scrp.contract_id
                    where scrp.customer_id = ?
   #end
   #sql("queryByNumCustomerId")
     SELECT * from 72crm_crm_receivables_plan WHERE num = ? and contract_id = ? and customer_id = ?
   #end
   #sql("queryByCustomerIdContractId")
     SELECT * from 72crm_crm_receivables_plan WHERE receivables_id is null and contract_id = ? and customer_id = ?
   #end
   #sql("queryReceivablesPlanById")
     select * from 72crm_crm_receivables_plan where contract_id = ?
   #end

  #sql ("deleteByIds")
    delete from 72crm_crm_receivables_plan where receivables_id = ?
  #end
    #sql("queryReceivablesReceivablesId")
     select * from 72crm_crm_receivables_plan where receivables_id in(
                #for(i : receivablesIds)
                    #(for.index > 0 ? "," : "")#para(i)
               #end
     )
   #end
  #sql ("queryUpdateField")
  select a.customer_id,b.customer_name,a.contract_id,c.num,a.money,a.return_date,a.return_type,a.remind,a.remark
  from 72crm_crm_receivables_plan as a left join 72crm_crm_customer as b on a.customer_id = b.customer_id
  left join 72crm_crm_contract as c on a.contract_id = c.contract_id
  where a.plan_id = ?
  #end
  
  #sql("queryPage")
    select scrp.plan_id,  scrp.num,scc.customer_name ,scco.num as contract_id ,
    user2.realname handPersonName,
    scrp.remind,
     scrp.money,scrp.return_date,return_type,
     scrp.remind,scrp.remark,`z`.`客户公司` AS customerCompanyName
         from 72crm_crm_receivables_plan as scrp
        LEFT JOIN 72crm_crm_customer as scc on scc.customer_id = scrp.customer_id
        LEFT JOIN 72crm_crm_contract as scco on scco.contract_id = scrp.contract_id
        left join 72crm_admin_user user2 on scco.owner_user_id = user2.user_id
         LEFT JOIN `fieldcustomerview` `z` ON (
			(
				CONVERT (`scc`.`batch_id` USING utf8mb4) = `z`.`field_batch_id`
			)
		)    
        where  1=1
	    #if(contractId)
	      and scco.num = #para(contractId)
	    #end
	     #if(customerName)
	      and scc.customer_name = #para(customerName)
	    #end
	    #if(handPersonName)
	      and user2.realname = #para(handPersonName)
	    #end
	    #if(startDate)
	      and scrp.return_date >= #para(startDate)
	    #end
	    #if(endDate)
	      and scrp.return_date <= #para(endDate)
	    #end
	#end	    
#end
