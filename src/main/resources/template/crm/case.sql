#namespace("crm.case")
    #sql ("queryByCaseId")
        select * from caseview where case_id = ?
    #end

    #sql ("getRecord")
        select a.record_id,b.img as user_img,b.realname,a.create_time,a.content,a.category,a.next_time,a.batch_id,a.business_ids,a.contacts_ids
        from 72crm_admin_record as a inner join 72crm_admin_user as b
        where a.create_user_id = b.user_id and types = 'crm_case' and types_id = ? order by a.create_time desc
    #end

    #sql ("updateCheckStatusById")
    update 72crm_crm_case set check_status = ? where case_id = ?
    #end

#end