#namespace("cost.manage")
  #sql("selectPage")
    select a.*, user2.realname hand_person_name,casee.num case_num from js_cost_manage a 
    left join 72crm_crm_case casee on a.case_id = casee.case_id
    left join 72crm_admin_user user2 on casee.owner_user_id = user2.user_id
    where  1=1
    #if(contract_id)
      and a.contract_id = #para(contract_id)
    #end
    #if(case_num)
      and casee.num = #para(case_num)
    #end
    #if(case_id)
      and casee.case_id = #para(case_id)
    #end
  #end
  
  #sql("selectById")
    select a.*,casee.name case_name,casee.num case_num,
    examine.name module_name from js_cost_manage a 
    left join 72crm_crm_case casee on a.case_id = casee.case_id
    left join 72crm_admin_examine examine on a.module_id = examine.examine_id
    where a.id = ?
  #end
  
  
#end
