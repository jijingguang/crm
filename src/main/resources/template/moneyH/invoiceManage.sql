#namespace("invoice.manage")
  #sql("selectPage")
    select a.*,user1.realname create_user_name,user2.realname hand_person_name,
    casee.name case_name,casee.num case_num
    from js_invoice_manage a 
    left join 72crm_admin_user user1 on a.hand_person_id = user1.user_id
    left join 72crm_crm_case casee on a.case_id = casee.case_id
    left join 72crm_admin_user user2 on casee.owner_user_id = user2.user_id
    where  1=1
    #if(bill_type)
      and a.bill_type = #para(bill_type)
    #end
    #if(case_id)
      and casee.case_id = #para(case_id)
    #end
    #if(case_num)
      and casee.num like CONCAT('%',#para(case_num),'%')
    #end
    #if(case_name)
      and casee.name like CONCAT('%',#para(case_name),'%')
    #end
    #if(hand_person_name)
      and user2.realname like CONCAT('%',#para(hand_person_name),'%')
    #end
    order by a.create_time desc
  #end
  
  
  #sql("selectById")
    select a.*,user1.realname hand_person_name,casee.name case_name from js_invoice_manage a 
    left join 72crm_admin_user user1 on a.hand_person_id = user1.user_id
    left join 72crm_crm_case casee on a.case_id = casee.case_id
    where a.id = ?
  #end
  
#end
