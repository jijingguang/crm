#namespace("remittance.plan")
  #sql("selectPageShare")
    select a.*,cust.customer_name customer_name,
    userr.realname hand_person_name,plan.return_date,con.num contract_num,
    `z`.`客户公司` AS customerCompanyName
    from js_remittance_plan a 
    left join 72crm_crm_contract con on a.contract_id = con.contract_id
    left join 72crm_crm_customer cust on cust.customer_id = con.customer_id
    left join 72crm_admin_user userr on userr.user_id = con.owner_user_id
    left join 72crm_crm_receivables_plan plan on con.contract_id = plan.contract_id
              and plan.num = a.number_of_periods
    LEFT JOIN `fieldcustomerview` `z` ON (
			(
				CONVERT (`cust`.`batch_id` USING utf8mb4) = `z`.`field_batch_id`
			)
		)          
    where  1=1
    #if(contractId)
      and con.contract_id = #para(contractId)
    #end
    #if(contractNum)
      and con.num = #para(contractNum)
    #end
    #if(customerName)
      and cust.customer_name = #para(customerName)
    #end
    #if(handPersonName)
      and userr.realname = #para(handPersonName)
    #end
    #if(startDate)
      and a.money_back_date >= #para(startDate)
    #end
    #if(endDate)
      and a.money_back_date <= #para(endDate)
    #end
    #if(case_id)
      and a.case_id = #para(case_id)
    #end
  #end
  
  #sql("selectById")
    select a.*,cust.customer_name customer_name,
    con.num contract_num,
    examine.name module_name from js_remittance_plan a 
    left join 72crm_crm_contract con on a.contract_id = con.contract_id
    left join 72crm_crm_customer cust on cust.customer_id = con.customer_id
    left join 72crm_admin_examine examine on a.module_id = examine.examine_id
    where a.id = ?
  #end
  
  
   #sql("selectPageContract")
    SELECT
		contract.contract_id contract_id,
		contract.num contract_num,
		contract.name,customer.customer_name
	FROM
		72crm_crm_contract contract
	JOIN 72crm_crm_customer customer ON contract.customer_id = customer.customer_id
	where  1=1
    #if(contractId)
      and contract.num = #para(contractId)
    #end
    #if(name)
      and contract.name = #para(name)
    #end
    #if(customerName)
      and customer.customer_name = #para(customerName)
    #end
  #end
  
  
  #sql("selectPageCase")
    SELECT
		casee.case_id case_id,casee.num case_num,casee.name case_name,
		contract.num contract_id,contract.name contract_name,
		userr.realname
	FROM
		72crm_crm_case casee
	JOIN 72crm_crm_contract contract ON contract.contract_id = casee.contract_id
	JOIN 72crm_admin_user userr ON userr.user_id = casee.owner_user_id
	where  1=1
    #if(caseName)
      and casee.name = #para(caseName)
    #end
    #if(caseId)
      and casee.num = #para(caseId)
    #end
    #if(contractName)
      and contract.name = #para(contractName)
    #end
  #end
  
#end
