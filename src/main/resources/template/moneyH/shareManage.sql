#namespace("share.manage")
  #sql("selectPageShare")
    select a.* from js_share_manage a 
    where  1=1
    #if(title)
      and a.title like CONCAT('%',#para(title),'%')
    #end
    #if(type)
      and a.type = #para(type)
    #end
  #end
  
  #sql("selectById")
    select * from js_share_manage a where  1=1 and a.id = ?
  #end
#end
