package com.kakarote.crm9.erp.admin.service;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.erp.admin.entity.AdminContract;
import com.kakarote.crm9.erp.admin.entity.AdminScene;
import com.kakarote.crm9.utils.R;

import cn.hutool.core.date.DateUtil;

public class AdminContractService {

    /**
     * @author wyq
     * 新增
     */
    public R addOrUpdate(JSONObject jsonObject) {
    	AdminContract adminContract = jsonObject.getObject("entity", AdminContract.class);
        
    	adminContract.setCreateTime(DateUtil.date());
        boolean save = adminContract.save();
        return save ? R.ok(): R.error();
    }
    
    
    public R contractGetPageList(BasePageRequest basePageRequest) {
        JSONObject jsonObject = basePageRequest.getJsonObject();
        JSONObject data = new JSONObject();
        if (jsonObject.getJSONObject("data") != null) {
            if (data != null) {
                jsonObject.getJSONObject("data").putAll(data);
            }
        } else {
            jsonObject.put("data", data);
        }
        basePageRequest.setJsonObject(jsonObject);
        List<Record> recordList = Db.find(Db.getSqlPara("admin.user.queryContract"));
        return R.ok().put("data", recordList);
    }
}
