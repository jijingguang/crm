package com.kakarote.crm9.erp.admin.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.kakarote.crm9.common.annotation.Permissions;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.erp.admin.service.AdminContractService;

public class AdminContractController extends Controller {
    @Inject
    private AdminContractService adminContractService;



    /**
     * @author wyq
     * 新增或更新商机
     */
    @Permissions({"con:contract:save","con:contract:update"})
    public void addOrUpdate(){
        JSONObject jsonObject = JSON.parseObject(getRawData());
        renderJson(adminContractService.addOrUpdate(jsonObject));
    }
    
    
    @Permissions({"con:contract:index"})
    public void queryPageList(BasePageRequest basePageRequest){
        JSONObject jsonObject = basePageRequest.getJsonObject();
        basePageRequest.setJsonObject(jsonObject);
        renderJson(adminContractService.contractGetPageList(basePageRequest));
    }


}
