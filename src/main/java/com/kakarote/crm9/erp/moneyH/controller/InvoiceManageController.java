package com.kakarote.crm9.erp.moneyH.controller;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.core.paragetter.Para;
import com.kakarote.crm9.common.annotation.Permissions;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.erp.moneyH.entity.InvoiceManage;
import com.kakarote.crm9.erp.moneyH.service.InvoiceManageService;

/**
 * 审批流程
 * 
 * @author hyf
 */
public class InvoiceManageController extends Controller {
	@Inject
	private InvoiceManageService service;


	@Permissions("bi:InvoiceManage:save")
	public void add(@Para("") InvoiceManage record) {
		renderJson(service.add(record));
	}

	@Permissions("bi:InvoiceManage:update")
	public void update(@Para("") InvoiceManage record) {
		renderJson(service.update(record));
	}

	@Permissions("bi:InvoiceManage:delete")
	public void delete() {
		Integer id = getInt("id");
		renderJson(service.delete(id));
	}

	@Permissions("bi:InvoiceManage:read")
	public void selectById() {
		Integer id = getInt("id");
		renderJson(service.selectById(id));
	}

	@Permissions("bi:InvoiceManage:index")
	public void queryPage(BasePageRequest<InvoiceManage> basePageRequest) {
		renderJson(service.queryPage(basePageRequest));
	}
	
}
