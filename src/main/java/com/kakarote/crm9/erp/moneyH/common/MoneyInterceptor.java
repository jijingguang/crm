package com.kakarote.crm9.erp.moneyH.common;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

public class MoneyInterceptor implements Interceptor {
    @Override
    public void intercept(Invocation invocation) {
        invocation.invoke();
    }
}
