package com.kakarote.crm9.erp.moneyH.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.upload.UploadFile;
import com.kakarote.crm9.erp.admin.entity.AdminFile;
import com.kakarote.crm9.erp.admin.service.AdminFileService;
import com.kakarote.crm9.erp.moneyH.service.UploadService;
import com.kakarote.crm9.utils.BaseUtil;
import com.kakarote.crm9.utils.R;

import cn.hutool.core.util.IdUtil;

/**
 * 审批流程
 * 
 * @author hyf
 */
public class UploadController extends Controller {
	@Inject
	private UploadService uploadService;
	@Inject
    private AdminFileService adminFileService;
	
	/**
	 * 获取上传多个的batchId
	 */
	public void getBatchId(){
        renderJson(R.ok(IdUtil.simpleUUID()));
    }

	/**
     * 上传多个
     * @author hmb
     */
    public void uploadMultiple(){
//    	String batchId = get("batchId");
    	HttpServletRequest request = getRequest();
//    	String batchId = request.getHeader("Admin-Token");
    	String parameter = request.getParameter("batchId");
    	String batchId = get("batchId");
        String prefix=BaseUtil.getDate();
        UploadFile file = getFile("file", prefix);
        R r=uploadService.upload(file,batchId,"file","/"+prefix);
        renderJson(r);
    }
    
    
    /**
     * 上传一个
     * @author hmb
     */
    public void upload(){
        String prefix=BaseUtil.getDate();
        UploadFile file = getFile("file", prefix);
        R r=uploadService.upload(file,null,"file","/"+prefix);
        renderJson(r);
    }
    
    
    
    public void download() throws FileNotFoundException, IOException {
    	HttpServletResponse response = getResponse();
    	
    	String id = get("id");
    	R r = adminFileService.queryById(id);
    	AdminFile adminFile = (AdminFile)r.get("data");
    	
    	// 1.获取要下载的文件的绝对路径
    	String realPath = adminFile.getPath();
    	
//        String realPath = this.getServletContext().getRealPath("/static/download/莱绅-价值观.png");
        // 2.获取要下载的文件名
        String fileName = realPath.substring(realPath.lastIndexOf(File.separator) + 1);
        // 3.设置content-disposition响应头控制浏览器以下载的形式打开文件
        response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        // 4.获取要下载的文件输入流
        InputStream in = new FileInputStream(realPath);
        int len = 0;
        // 5.创建数据缓冲区
        byte[] buffer = new byte[1024];
        // 6.通过response对象获取OutputStream流
        OutputStream out = response.getOutputStream();
        // 7.将FileInputStream流写入到buffer缓冲区
        while ((len = in.read(buffer)) > 0) {
            // 8.使用OutputStream将缓冲区的数据输出到客户端浏览器
            out.write(buffer, 0, len);
        }
        in.close();
    }
}
