package com.kakarote.crm9.erp.moneyH.controller;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.core.paragetter.Para;
import com.kakarote.crm9.common.annotation.Permissions;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.erp.moneyH.entity.CostManage;
import com.kakarote.crm9.erp.moneyH.service.CostManageService;

/**
 * 审批流程
 * 
 * @author hyf
 */
public class CostManageController extends Controller {
	@Inject
	private CostManageService service;

	@Permissions("bi:CostManage:save")
	public void add(@Para("") CostManage record) {
		renderJson(service.add(record));
	}

	@Permissions("bi:CostManage:update")
	public void update(@Para("") CostManage record) {
		renderJson(service.update(record));
	}
	
	@Permissions("bi:CostManage:delete")
	public void delete() {
		Integer id = getInt("id");
		renderJson(service.delete(id));
	}
	
	@Permissions("bi:CostManage:read")
	public void selectById() {
		Integer id = getInt("id");
		renderJson(service.selectById(id));
	}
	
	@Permissions("bi:CostManage:index")
	public void queryPage(BasePageRequest<CostManage> basePageRequest) {
		renderJson(service.queryPage(basePageRequest));
	}
	
}
