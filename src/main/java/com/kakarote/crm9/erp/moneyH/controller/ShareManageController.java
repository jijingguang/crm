package com.kakarote.crm9.erp.moneyH.controller;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.core.paragetter.Para;
import com.kakarote.crm9.common.annotation.Permissions;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.erp.moneyH.entity.ShareManage;
import com.kakarote.crm9.erp.moneyH.service.ShareManageService;

/**
 * 审批流程
 * 
 * @author hyf
 */
public class ShareManageController extends Controller {
	@Inject
	private ShareManageService service;

	@Permissions("bi:shareManage:save")
	public void add(@Para("") ShareManage record) {
//		String ratio = record.getRatio();
//		if(ratio!=null||!ratio.endsWith("%")) {
//			record.setRatio(ratio.concat("%"));
//		}
		renderJson(service.add(record));
	}

	@Permissions("bi:shareManage:update")
	public void update(@Para("") ShareManage record) {
//		String ratio = record.getRatio();
//		if(ratio!=null||!ratio.endsWith("%")) {
//			record.setRatio(ratio.concat("%"));
//		}
		renderJson(service.update(record));
	}

	@Permissions("bi:shareManage:delete")
	public void delete() {
		Integer id = getInt("id");
		renderJson(service.delete(id));
	}

	@Permissions("bi:shareManage:read")
	public void selectById() {
		Integer id = getInt("id");
		renderJson(service.selectById(id));
	}

	@Permissions("bi:shareManage:index")
	public void queryPage(BasePageRequest<ShareManage> basePageRequest) {
		
		renderJson(service.queryPage(basePageRequest));
	}
	
//	@Permissions("bi:shareManage:checkPass")
	public void checkPass() {
		Integer id = getInt("id");
		renderJson(service.checkPass(id));
	}
	
//	@Permissions("bi:shareManage:checkReturn")
	public void checkReturn() {
		Integer id = getInt("id");
		renderJson(service.checkReturn(id));
	}
	

}
