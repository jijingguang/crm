package com.kakarote.crm9.erp.moneyH.service;

import com.jfinal.aop.Before;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.erp.moneyH.entity.CostManage;
import com.kakarote.crm9.erp.moneyH.entity.InvoiceManage;
import com.kakarote.crm9.erp.moneyH.entity.ShareManage;
import com.kakarote.crm9.utils.BaseUtil;
import com.kakarote.crm9.utils.R;

import cn.hutool.core.date.DateUtil;

public class CostManageService {
    
    public R add(CostManage record) {
//    	record.setCostDate(record.getCostDate().substring(0, 10));
    	record.setCreateUserId(BaseUtil.getUser().getUserId());
    	record.setCreateTime(DateUtil.date());
    	return record.save() ? R.ok() : R.error();
	}

	public R update(CostManage record) {
//    	record.setCostDate(record.getCostDate().substring(0, 10));
		record.setUpdateUserId(BaseUtil.getUser().getUserId());
    	record.setUpdateTime(DateUtil.date());
    	return record.update() ? R.ok() : R.error();
	}
	
	@Before(Tx.class)  //注解，支持事务。
	public R delete(Integer id) {
		CostManage record = new CostManage();
		record.setId(id);
    	return record.delete() ? R.ok() : R.error();  //主键删除  不支持事务
	}

	public R selectById(Integer id) {
//		CostManage record = new CostManage();
//		record = record.findById(id);
//    	return R.success(record);
		 Record findFirst = Db.findFirst(Db.getSql("cost.manage.selectById"),id);
//		InvoiceManage findFirst = InvoiceManage.dao.findFirst(Db.getSql("cost.manage.selectById"), id);
    	return R.success(findFirst);
	}

	public R queryPage(BasePageRequest<CostManage> basePageRequest) {
		CostManage data = basePageRequest.getData();
		String contractId = data.getContractId();
		String caseId = data.getCaseId();
		
//		String billType = data.getBillType();
//		String caseId = data.getCaseId();
//		String handPersonId = data.getHandPersonId();
        Page<Record> page = Db.paginate(basePageRequest.getPage(), basePageRequest.getLimit(), 
        		Db.getSqlPara("cost.manage.selectPage",Kv.by("contract_id", contractId)
        				.set("case_id", caseId)));
        return R.ok().put("data", page);
	}
	
    
}
