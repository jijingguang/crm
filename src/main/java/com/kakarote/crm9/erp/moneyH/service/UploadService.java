package com.kakarote.crm9.erp.moneyH.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.config.Constants;
import com.jfinal.upload.UploadFile;
import com.kakarote.crm9.erp.admin.entity.AdminFile;
import com.kakarote.crm9.utils.BaseUtil;
import com.kakarote.crm9.utils.R;

import cn.hutool.core.util.ClassLoaderUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;

public class UploadService {
	
	/**
     * @param file    文件
     * @param batchId 批次ID
     */
    public R upload(UploadFile file, String batchId,String fileType,String prefix) {
        if (batchId == null || "".equals(batchId)) {
            batchId = IdUtil.simpleUUID();
        }
        AdminFile adminFile = new AdminFile();
        adminFile.setBatchId(batchId);
        adminFile.setCreateTime(new Date());
        adminFile.setCreateUserId(BaseUtil.getUser().getUserId().intValue());
        adminFile.setPath(file.getFile().getAbsolutePath());
        if(ClassLoaderUtil.isPresent("com.jfinal.server.undertow.UndertowServer")){
            adminFile.setFilePath(BaseUtil.getIpAddress() + prefix + "/" + file.getFileName());
        }else {
            adminFile.setFilePath(BaseUtil.getIpAddress()+new Constants().getBaseUploadPath()+"/"+ prefix + "/" + file.getFileName());
        }
        adminFile.setName(file.getFileName());
        if(StrUtil.isNotBlank(fileType)){
            adminFile.setFileType(fileType);
        }
        adminFile.setSize(file.getFile().length());
//        put("file_id",adminFile.getFileId()
        return adminFile.save() ? R.ok().put("batchId", batchId).put("name",file.getFileName()).put("url", adminFile.getFilePath()).put("size",file.getFile().length()/1000+"KB") : R.error();
    }
	
}
