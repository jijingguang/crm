package com.kakarote.crm9.erp.moneyH.controller;

import java.text.ParseException;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.core.paragetter.Para;
import com.kakarote.crm9.common.annotation.Permissions;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.erp.crm.service.CrmReceivablesPlanService;
import com.kakarote.crm9.erp.moneyH.entity.JsReceStaff;
import com.kakarote.crm9.erp.moneyH.entity.RemittancePlan;
import com.kakarote.crm9.erp.moneyH.service.RemittancePlanService;
import com.kakarote.crm9.erp.moneyH.service.SumMoneyService;

/**
 * 审批流程
 * 
 * @author hyf
 */
public class RemittancePlanController extends Controller {
	@Inject
	private RemittancePlanService service;
	@Inject
    private CrmReceivablesPlanService crmReceivablesPlanService;
	@Inject
	private SumMoneyService sumMoneyService;

//	@Permissions("bi:RemittancePlan:save")
	public void add(@Para("") RemittancePlan record) {
		renderJson(service.add(record));
	}

//	@Permissions("bi:RemittancePlan:update")
	public void update(@Para("") RemittancePlan record) {
		renderJson(service.update(record));
	}

//	@Permissions("bi:RemittancePlan:delete")
	public void delete() {
		String id = get("id");
		renderJson(service.delete(id));
	}

	@Permissions("bi:RemittancePlan:read")
	public void selectById() {
		String id = get("id");
		renderJson(service.selectById(id));
	}

	@Permissions("bi:RemittancePlan:index")
	public void queryPage(BasePageRequest<RemittancePlan> basePageRequest) throws ParseException {
		renderJson(service.queryPage(basePageRequest));
	}
	public void selectPageContract(BasePageRequest<RemittancePlan> basePageRequest) {
		renderJson(service.selectPageContract(basePageRequest));
	}
	public void selectPageCase(BasePageRequest<RemittancePlan> basePageRequest) {
		renderJson(service.selectPageCase(basePageRequest));
	}
	
	@Permissions("bi:RemittancePlanList:index")
    public void queryPagePlan(BasePageRequest<Object> basePageRequest) {
		renderJson(crmReceivablesPlanService.queryPagePlan(basePageRequest));
	}
	
	public void addSumMoney(@Para("") JsReceStaff record) {
		renderJson(sumMoneyService.sumMoney(record));
	}
	
}
