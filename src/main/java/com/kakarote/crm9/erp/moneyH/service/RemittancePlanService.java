package com.kakarote.crm9.erp.moneyH.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.erp.moneyH.entity.RemittancePlan;
import com.kakarote.crm9.erp.moneyH.entity.RemittancePlan;
import com.kakarote.crm9.erp.moneyH.entity.ShareManage;
import com.kakarote.crm9.utils.BaseUtil;
import com.kakarote.crm9.utils.R;

import cn.hutool.core.date.DateUtil;

public class RemittancePlanService {
    
    public R add(RemittancePlan record) {
//    	record.setMoneyBackDate(record.getMoneyBackDate().substring(0, 10));
    	record.setId("HK"+System.currentTimeMillis());
    	record.setCreateUserId(BaseUtil.getUser().getUserId());
    	record.setCreateTime(DateUtil.date());
    	return record.save() ? R.ok() : R.error();
	}

	public R update(RemittancePlan record) {
//    	record.setMoneyBackDate(record.getMoneyBackDate().substring(0, 10));
		record.setUpdateUserId(BaseUtil.getUser().getUserId());
    	record.setUpdateTime(DateUtil.date());
    	return record.update() ? R.ok() : R.error();
	}
	
	@Before(Tx.class)  //注解，支持事务。
	public R delete(String id) {
		RemittancePlan record = new RemittancePlan();
		record.setId(id);
    	return record.delete() ? R.ok() : R.error();  //主键删除  不支持事务
	}

	public R selectById(String id) {
//		RemittancePlan record = new RemittancePlan();
//		record = record.findById(id);
//    	return R.success(record);
		
		Record findFirst = Db.findFirst(Db.getSql("remittance.plan.selectById"),id);
//		RemittancePlan findFirst = RemittancePlan.dao.findFirst(Db.getSql("remittance.plan.selectById"), id);
    	return R.success(findFirst);
	}

	public R queryPage(BasePageRequest<RemittancePlan> basePageRequest) throws ParseException {
		RemittancePlan data = basePageRequest.getData();
		String contractId = data.getContractId();
		String customerName = data.getCustomerName();
		String handPersonName = data.getHandPersonName();
		
		JSONObject jsonObject = basePageRequest.getJsonObject();
		String startDate = (String)jsonObject.get("startDate");
		String endDate = (String)jsonObject.get("endDate");
		String contractNum = (String)jsonObject.get("contractNum");
		if(startDate!=null) {
			startDate = startDate.substring(0, 10);
		}
		if(startDate!=null) {
			endDate = endDate.substring(0, 10);
		}
		
		
		
        Page<Record> page = Db.paginate(basePageRequest.getPage(), basePageRequest.getLimit(), 
        		Db.getSqlPara("remittance.plan.selectPageShare",Kv.by("contractId", contractId)
        				.set("customerName", customerName)
        				.set("startDate", startDate)
        				.set("endDate", endDate)
        				.set("handPersonName", handPersonName)
        				.set("contractNum", contractNum)));
        
        List<Record> list = page.getList();
		for (Record record : list) {
			Date return_date = (Date)record.get("return_date");
			String money_back_date = (String)record.get("money_back_date");
			
			record.set("remit_status", this.compareDate(money_back_date, return_date));
		}
		
        return R.ok().put("data", page);
	}
	
	public String compareDate(String date1,Date date2) throws ParseException {
		if(date2==null) {
			return "";
		}
		
		SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd" );
		Date parse1 = sdf.parse(date1);
//		Date parse2 = sdf.parse(date2);
		
		if(parse1.compareTo(date2)==1) {
			return "逾期";
		}else {
			return "正常";
		}
	}
	
//	public static void main(String[] args) {
//		if("2017-11-11">"2017-11-11")
//	}
	
	public R selectPageContract(BasePageRequest<RemittancePlan> basePageRequest) {
//		RemittancePlan data = basePageRequest.getData();
//		String contractId = data.getContractId();
		
		JSONObject jsonObject = basePageRequest.getJsonObject();
		String contractId = (String)jsonObject.get("contractId");
		String name = (String)jsonObject.get("name");
		String customerName = (String)jsonObject.get("customerName");
		Page<Record> page = Db.paginate(basePageRequest.getPage(), basePageRequest.getLimit(), 
				Db.getSqlPara("remittance.plan.selectPageContract",Kv.by("contractId", contractId)
						.set("name", name).set("customerName", customerName)));
		return R.ok().put("data", page);
	}
	
	public R selectPageCase(BasePageRequest<RemittancePlan> basePageRequest) {
//		RemittancePlan data = basePageRequest.getData();
//		String contractId = data.getContractId();
		JSONObject jsonObject = basePageRequest.getJsonObject();
		String caseName = (String)jsonObject.get("caseName");
		String caseId = (String)jsonObject.get("caseId");
		String contractName = (String)jsonObject.get("contractName");
		Page<Record> page = Db.paginate(basePageRequest.getPage(), basePageRequest.getLimit(), 
				Db.getSqlPara("remittance.plan.selectPageCase",Kv.by("caseName", caseName)
						.set("caseId", caseId)
						.set("contractName", contractName)));
		return R.ok().put("data", page);
	}
	
	
    
}
