package com.kakarote.crm9.erp.moneyH.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.erp.admin.service.AdminFileService;
import com.kakarote.crm9.erp.moneyH.entity.InvoiceManage;
import com.kakarote.crm9.erp.moneyH.entity.InvoiceManage;
import com.kakarote.crm9.erp.moneyH.entity.ShareManage;
import com.kakarote.crm9.utils.BaseUtil;
import com.kakarote.crm9.utils.R;

import cn.hutool.core.date.DateUtil;

public class InvoiceManageService {
	
	@Inject
	private AdminFileService adminFileService;
    
    public R add(InvoiceManage record) {
//    	record.setInvoiceDate(record.getInvoiceDate().substring(0, 10));
    	record.setCreateUserId(BaseUtil.getUser().getUserId());
    	record.setCreateTime(DateUtil.date());
    	return record.save() ? R.ok() : R.error();
	}

	public R update(InvoiceManage record) {
//		record.setInvoiceDate(record.getInvoiceDate().substring(0, 10));
		record.setUpdateUserId(BaseUtil.getUser().getUserId());
    	record.setUpdateTime(DateUtil.date());
    	return record.update() ? R.ok() : R.error();
	}
	
	@Before(Tx.class)  //注解，支持事务。
	public R delete(Integer id) {
		InvoiceManage record = new InvoiceManage();
		record.setId(id);
		record = record.findById(id);
		if(record!=null) {
			String batchId = record.getAnnexId();
			if(batchId!=null) {
				adminFileService.removeByBatchId(batchId);
			}
		}
		
    	return record.delete() ? R.ok() : R.error();  //主键删除  不支持事务
	}

	public R selectById(Integer id) {
//		InvoiceManage record = new InvoiceManage();
//		record = record.findById(id);
		 Record findFirst = Db.findFirst(Db.getSql("invoice.manage.selectById"),id);
//		InvoiceManage findFirst = InvoiceManage.dao.findFirst(Db.getSql("invoice.manage.selectById"), id);
    	return R.success(findFirst);
	}

	public R queryPage(BasePageRequest<InvoiceManage> basePageRequest) {
		InvoiceManage data = basePageRequest.getData();
		String billType = data.getBillType();
		String caseId = data.getCaseId();
		String caseName = data.getCaseName();
		String handPersonName = data.getHandPersonName();
		
		JSONObject jsonObject = basePageRequest.getJsonObject();
		Object caseNum = jsonObject.get("caseNum");
		
        Page<Record> page = Db.paginate(basePageRequest.getPage(), basePageRequest.getLimit(), 
        		Db.getSqlPara("invoice.manage.selectPage",Kv.by("bill_type", billType)
        				.set("case_id", caseId).set("case_name", caseName)
        				.set("hand_person_name", handPersonName)
        				.set("case_num", caseNum)));
        return R.ok().put("data", page);
	}
	
    
}
