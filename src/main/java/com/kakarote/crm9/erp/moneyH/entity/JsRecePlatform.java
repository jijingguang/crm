package com.kakarote.crm9.erp.moneyH.entity;

import com.kakarote.crm9.erp.moneyH.entity.base.BaseJsRecePlatform;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class JsRecePlatform extends BaseJsRecePlatform<JsRecePlatform> {
	public static final JsRecePlatform dao = new JsRecePlatform().dao();
}
