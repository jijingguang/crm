package com.kakarote.crm9.erp.moneyH.controller;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.core.paragetter.Para;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.erp.moneyH.entity.CostManage;
import com.kakarote.crm9.erp.moneyH.service.AdminFieldvService;
import com.kakarote.crm9.erp.moneyH.service.CostManageService;

/**
 * 审批流程
 * 
 * @author hyf
 */
public class AdminFieldvController extends Controller {
	@Inject
	private AdminFieldvService service;


	public void selectByFieldId() {
		Integer id = getInt("fieldId");
		renderJson(service.selectByFieldId(id));
	}

	
}
