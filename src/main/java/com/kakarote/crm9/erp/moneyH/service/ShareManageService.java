package com.kakarote.crm9.erp.moneyH.service;

import com.jfinal.aop.Before;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.erp.moneyH.common.CommConst;
import com.kakarote.crm9.erp.moneyH.entity.ShareManage;
import com.kakarote.crm9.utils.BaseUtil;
import com.kakarote.crm9.utils.R;

import cn.hutool.core.date.DateUtil;

public class ShareManageService {
    
    public R add(ShareManage record) {
    	record.setCreateUserId(BaseUtil.getUser().getUserId());
    	record.setCreateTime(DateUtil.date());
//    	record.setState(CommConst.state_0);
    	return record.save() ? R.ok() : R.error();
	}

	public R update(ShareManage record) {
		record.setUpdateUserId(BaseUtil.getUser().getUserId());
    	record.setUpdateTime(DateUtil.date());
    	return record.update() ? R.ok() : R.error();
	}
	
	@Before(Tx.class)  //注解，支持事务。
	public R delete(Integer id) {
		ShareManage record = new ShareManage();
		record.setId(id);
    	return record.delete() ? R.ok() : R.error();  //主键删除  不支持事务
	}

	public R selectById(Integer id) {
		ShareManage record = new ShareManage();
		record = record.findById(id);
    	return R.success(record);
	}
	
//	public R selectById(Integer id) {
//		ShareManage findFirst = ShareManage.dao.findFirst(Db.getSql("share.manage.selectById"), id);
//    	return R.success(findFirst);
//	}
	

	public R queryPage(BasePageRequest<ShareManage> basePageRequest) {
		ShareManage data = basePageRequest.getData();
		String type = data.getType();
		String title = data.getTitle();
        Page<Record> page = Db.paginate(basePageRequest.getPage(), basePageRequest.getLimit(), 
        		Db.getSqlPara("share.manage.selectPageShare",Kv.by("type", type).set("title", title)));
        return R.ok().put("data", page);
	}
	
	public R checkPass(Integer id) {
		ShareManage shareManage = new ShareManage();
		shareManage.setId(id);
		shareManage.setState(CommConst.state_1);
		return this.update(shareManage);
	}
	
	public R checkReturn(Integer id) {
		ShareManage shareManage = new ShareManage();
		shareManage.setId(id);
		shareManage.setState(CommConst.state_0);
		return this.update(shareManage);
	}
    
}
