package com.kakarote.crm9.erp.moneyH.service;

import java.math.BigDecimal;
import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.SqlPara;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.kakarote.crm9.erp.admin.entity.AdminDept;
import com.kakarote.crm9.erp.admin.entity.AdminUser;
import com.kakarote.crm9.erp.moneyH.entity.JsReceBusiness;
import com.kakarote.crm9.erp.moneyH.entity.JsRecePlatform;
import com.kakarote.crm9.erp.moneyH.entity.JsReceStaff;
import com.kakarote.crm9.erp.moneyH.entity.ShareManage;
import com.kakarote.crm9.utils.BaseUtil;
import com.kakarote.crm9.utils.R;

import cn.hutool.core.date.DateUtil;

public class SumMoneyService {
	/**
	 * 进行计算，插入数据。
	 */
	@Before(Tx.class) 
	public R sumMoney(JsReceStaff staff) {
		
		staff.setCreateUserId(BaseUtil.getUser().getUserId());
		staff.setCreateTime(DateUtil.date());
		
		Integer ownerUserId = staff.getOwnerUserId();
		AdminUser adminUser = AdminUser.dao.findById(ownerUserId);
		Integer deptId = adminUser.getDeptId();
		
		Integer pid = -1;
		while(!pid.equals(new Integer(1))) {
			AdminDept dept = AdminDept.dao.findById(deptId);
			pid = dept.getPid();
			deptId = dept.getDeptId();
			
			if(pid.equals(new Integer(0))) {
				break;
			}
		}
		
		//取得分润。
		SqlPara sqlPara = new SqlPara();
		sqlPara.setSql("select * from js_share_manage where department = "+deptId);
		List<ShareManage> list = ShareManage.dao.find(sqlPara);
		int i = 0;
		for (ShareManage shareManage : list) {
			BigDecimal money = staff.getMoney();
			String type = shareManage.getType();
			
			if("员工提成".equals(type)) {
				++i;
				String ratio = shareManage.getRatio();
				BigDecimal rate = new BigDecimal(ratio).divide(new BigDecimal(100));
				staff.setJsMoney(money.multiply(rate));
				staff.setRate(ratio);
				staff.save();
			}else if("平台提成".equals(type)) {
				++i;
				String ratio = shareManage.getRatio();
				BigDecimal rate = new BigDecimal(ratio).divide(new BigDecimal(100));
				JsRecePlatform jsRecePlatform = new JsRecePlatform();
				copyJsRecePlatform(jsRecePlatform, staff);
				jsRecePlatform.setJsMoney(money.multiply(rate));
				jsRecePlatform.setRate(ratio);
				jsRecePlatform.save();
			}else if("商家提成".equals(type)) {
				++i;
				String ratio = shareManage.getRatio();
				BigDecimal rate = new BigDecimal(ratio).divide(new BigDecimal(100));
				JsReceBusiness jsReceBusiness = new JsReceBusiness();
				copyJsReceBusiness(jsReceBusiness, staff);
				jsReceBusiness.setJsMoney(money.multiply(rate));
				jsReceBusiness.setRate(ratio);
				jsReceBusiness.save();
			}
		}
		
		if(i==0) {
			staff.setRemarks("在分润中找不到对应的记录。");
			staff.save();
		}
		
		return R.ok();
	}
	
	
	/**
	 * 复制对象。平台
	 */
	public void copyJsRecePlatform(JsRecePlatform neww,JsReceStaff older) {
		neww.setReceivablesId(older.getReceivablesId());
		neww.setNumber(older.getNumber());
		neww.setCustomerId(older.getCustomerId());
		neww.setContractId(older.getContractId());
		neww.setReturnTime(older.getReturnTime());
		neww.setMoney(older.getMoney());
		neww.setRate(older.getRate());
		neww.setJsMoney(older.getJsMoney());
		neww.setOwnerUserId(older.getOwnerUserId());
		neww.setDeptId(older.getDeptId());
		neww.setCreateUserId(older.getCreateUserId());
		neww.setCreateTime(older.getCreateTime());
		neww.setRemarks(older.getRemarks());
		neww.setNum(older.getNum());
	}
	
	/**
	 * 复制对象。商家
	 */
	public void copyJsReceBusiness(JsReceBusiness neww,JsReceStaff older) {
		neww.setReceivablesId(older.getReceivablesId());
		neww.setNumber(older.getNumber());
		neww.setCustomerId(older.getCustomerId());
		neww.setContractId(older.getContractId());
		neww.setReturnTime(older.getReturnTime());
		neww.setMoney(older.getMoney());
		neww.setRate(older.getRate());
		neww.setJsMoney(older.getJsMoney());
		neww.setOwnerUserId(older.getOwnerUserId());
		neww.setDeptId(older.getDeptId());
		neww.setCreateUserId(older.getCreateUserId());
		neww.setCreateTime(older.getCreateTime());
		neww.setRemarks(older.getRemarks());
		neww.setNum(older.getNum());
	}
}
