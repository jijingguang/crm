package com.kakarote.crm9.erp.moneyH.common;

import com.jfinal.config.Routes;
import com.kakarote.crm9.erp.moneyH.controller.AdminFieldvController;
import com.kakarote.crm9.erp.moneyH.controller.CostManageController;
import com.kakarote.crm9.erp.moneyH.controller.InvoiceManageController;
import com.kakarote.crm9.erp.moneyH.controller.RemittancePlanController;
import com.kakarote.crm9.erp.moneyH.controller.ShareManageController;
import com.kakarote.crm9.erp.moneyH.controller.UploadController;

public class MoneyRouter extends Routes {
    @Override
    public void config() {
        addInterceptor(new MoneyInterceptor());
        
        
        add("/shareManage", ShareManageController.class);
        add("/invoiceManage", InvoiceManageController.class);
        add("/costManage", CostManageController.class);
        add("/remittancePlan", RemittancePlanController.class);
        add("/adminFieldv", AdminFieldvController.class);
        add("/upload", UploadController.class);
    }
}
