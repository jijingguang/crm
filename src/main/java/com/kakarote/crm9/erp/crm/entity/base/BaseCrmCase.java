package com.kakarote.crm9.erp.crm.entity.base;

import com.jfinal.plugin.activerecord.IBean;
import com.jfinal.plugin.activerecord.Model;

public abstract class BaseCrmCase<M extends BaseCrmCase<M>> extends Model<M> implements IBean {
    public void setCaseId(Integer caseId) {
        set("case_id", caseId);
    }

    public Integer getCaseId() {
        return getInt("case_id");
    }

    public void setNextTime(java.util.Date nextTime) {
        set("next_time", nextTime);
    }

    public java.util.Date getNextTime() {
        return get("next_time");
    }

    public void setCustomerId(Integer customerId) {
        set("customer_id", customerId);
    }

    public Integer getCustomerId() {
        return getInt("customer_id");
    }

    public void setContractId(Integer contractId) {
        set("contract_id", contractId);
    }

    public Integer getContractId() {
        return getInt("contract_id");
    }

    public void setBusinessId(Integer businessId) {
        set("business_id", businessId);
    }

    public Integer getBusinessId() {
        return getInt("business_id");
    }

    public void setName(String name) {
        set("name", name);
    }

    public String getName() {
        return getStr("name");
    }

    public void setMoney(java.math.BigDecimal money) {
        set("money", money);
    }

    public java.math.BigDecimal getMoney() {
        return get("money");
    }


    public void setRemark(String remark) {
        set("remark", remark);
    }

    public String getRemark() {
        return getStr("remark");
    }

    public void setCreateUserId(Integer createUserId) {
        set("create_user_id", createUserId);
    }

    public Integer getCreateUserId() {
        return getInt("create_user_id");
    }

    public void setOwnerUserId(Integer ownerUserId) {
        set("owner_user_id", ownerUserId);
    }

    public Integer getOwnerUserId() {
        return getInt("owner_user_id");
    }

    public void setCreateTime(java.util.Date createTime) {
        set("create_time", createTime);
    }

    public java.util.Date getCreateTime() {
        return get("create_time");
    }

    public void setUpdateTime(java.util.Date updateTime) {
        set("update_time", updateTime);
    }

    public java.util.Date getUpdateTime() {
        return get("update_time");
    }

    public void setBatchId(String batchId) {
        set("batch_id", batchId);
    }

    public String getBatchId() {
        return getStr("batch_id");
    }

    public void setRoUserId(String roUserId) {
        set("ro_user_id", roUserId);
    }

    public String getRoUserId() {
        return getStr("ro_user_id");
    }

    public void setRwUserId(String rwUserId) {
        set("rw_user_id", rwUserId);
    }

    public String getRwUserId() {
        return getStr("rw_user_id");
    }

    public void setStatusRemark(String statusRemark) {
        set("status_remark", statusRemark);
    }

    public String getStatusRemark() {
        return getStr("status_remark");
    }

    public void setNum(String num) {
        set("num", num);
    }

    public String getNum() {
        return getStr("num");
    }

    public Integer getCheckStatus() {
        return getInt("check_status");
    }

    public void setCheckStatus(Integer checkStatus) {
        set("check_status", checkStatus);
    }


    public Integer getExamineRecordId() {
        return getInt("examine_record_id");
    }

    public void setExamineRecordId(Integer examineRecordId) {
        set("examine_record_id", examineRecordId);
    }

    public Integer getExamineId() {
        return getInt("examine_id");
    }

    public void setExamineId(Integer examineId) {
        set("examine_id", examineId);
    }

}
