package com.kakarote.crm9.erp.crm.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.core.paragetter.Para;
import com.kakarote.crm9.common.annotation.NotNullValidate;
import com.kakarote.crm9.common.annotation.Permissions;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.erp.admin.entity.AdminRecord;
import com.kakarote.crm9.erp.admin.service.AdminFieldService;
import com.kakarote.crm9.erp.admin.service.AdminSceneService;
import com.kakarote.crm9.erp.crm.common.CrmEnum;
import com.kakarote.crm9.erp.crm.entity.CrmCase;
import com.kakarote.crm9.erp.crm.entity.CrmReceivables;
import com.kakarote.crm9.erp.crm.entity.CrmReceivablesPlan;
import com.kakarote.crm9.erp.crm.service.*;
import com.kakarote.crm9.utils.AuthUtil;
import com.kakarote.crm9.utils.R;

public class CrmCaseController extends Controller {
    @Inject
    private CrmCustomerService crmCustomerService;

    @Inject
    private CrmContactsService crmContactsService;//联系人

    @Inject
    private CrmBusinessService crmBusinessService;//商机

    @Inject
    private CrmContractService crmContractService;//合同

    @Inject
    private AdminFieldService adminFieldService;

    @Inject
    private AdminSceneService adminSceneService;

    @Inject
    private CrmCaseService crmCaseService;

    @Inject
    private CrmReceivablesPlanService crmReceivablesPlanService;


    /**
     * 新增案件和修改
     */
    @Permissions({"crm:case:save", "crm:case:update"})
    public void addOrUpdate() {
        JSONObject jsonObject = JSON.parseObject(getRawData());
        renderJson(crmCaseService.addOrUpdate(jsonObject, "noImport"));
    }

    /**
     * @author wyq
     * 查看列表页
     */
    @Permissions({"crm:case:index"})
    public void queryPageList(BasePageRequest basePageRequest) {
        JSONObject jsonObject = basePageRequest.getJsonObject().fluentPut("type", 9);
        basePageRequest.setJsonObject(jsonObject);
        renderJson(adminSceneService.filterConditionAndGetPageList(basePageRequest));
    }

    @Permissions("crm:case:read")
    @NotNullValidate(value = "caseId", message = "案件id不能为空")
    public void queryById(@Para("caseId") Integer id) {
        renderJson(crmCaseService.queryById(id));
    }

    @Permissions("crm:case:delete")
    @NotNullValidate(value = "caseIds", message = "合同id不能为空")
    public void deleteByIds(@Para("caseIds") String caseIds) {
        renderJson(crmCaseService.deleteByIds(caseIds));
    }


    @NotNullValidate(value = "typesId", message = "案件id不能为空")
    @NotNullValidate(value = "content", message = "内容不能为空")
    @NotNullValidate(value = "category", message = "跟进类型不能为空")
    public void addRecord(@Para("") AdminRecord adminRecord) {
        boolean auth = AuthUtil.isCrmAuth(AuthUtil.getCrmTablePara(CrmEnum.CASE_TYPE_KEY.getSign()), adminRecord.getTypesId());
        if (auth) {
            renderJson(R.noAuth());
            return;
        }
        renderJson(crmCaseService.addRecord(adminRecord));
    }

    /**
     * @author wyq
     * 查看跟进记录
     */
    public void getRecord(BasePageRequest<CrmCase> basePageRequest) {
        boolean auth = AuthUtil.isCrmAuth(AuthUtil.getCrmTablePara(CrmEnum.CASE_TYPE_KEY.getSign()), basePageRequest.getData().getCaseId());
        if (auth) {
            renderJson(R.noAuth());
            return;
        }
        renderJson(R.ok().put("data", crmCaseService.getRecord(basePageRequest)));
    }

    public void queryByContract(BasePageRequest<CrmReceivables> basePageRequest) {
        renderJson(crmReceivablesPlanService.qureyListByContractId(basePageRequest));
    }

    //案件提交审核
    public void auditCase(@Para("caseId") Integer caseId) {
        renderJson(crmCaseService.auditCase(caseId));
    }
}
