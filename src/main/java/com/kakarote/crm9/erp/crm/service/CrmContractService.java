package com.kakarote.crm9.erp.crm.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.kakarote.crm9.common.annotation.NotNullValidate;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.common.constant.BaseConstant;
import com.kakarote.crm9.erp.admin.entity.*;
import com.kakarote.crm9.erp.admin.service.AdminExamineRecordService;
import com.kakarote.crm9.erp.admin.service.AdminFieldService;
import com.kakarote.crm9.erp.admin.service.AdminFileService;
import com.kakarote.crm9.erp.crm.common.CrmEnum;
import com.kakarote.crm9.erp.crm.entity.*;
import com.kakarote.crm9.erp.oa.common.OaEnum;
import com.kakarote.crm9.erp.oa.entity.OaEvent;
import com.kakarote.crm9.erp.oa.entity.OaEventRelation;
import com.kakarote.crm9.erp.oa.service.OaActionRecordService;
import com.kakarote.crm9.utils.AuthUtil;
import com.kakarote.crm9.utils.BaseUtil;
import com.kakarote.crm9.utils.FieldUtil;
import com.kakarote.crm9.utils.R;

import java.text.SimpleDateFormat;
import java.util.*;

public class CrmContractService {
    @Inject
    private AdminFieldService adminFieldService;

    @Inject
    private FieldUtil fieldUtil;

    @Inject
    private CrmRecordService crmRecordService;

    @Inject
    private AdminFileService adminFileService;

    @Inject
    private AdminExamineRecordService examineRecordService;

    @Inject
    private OaActionRecordService oaActionRecordService;

    @Inject
    private AuthUtil authUtil;

    /**
     * 分页条件查询合同
     */
    public Page<Record> queryPage(BasePageRequest<CrmContract> basePageRequest) {
        return Db.paginate(basePageRequest.getPage(), basePageRequest.getLimit(), Db.getSqlPara("crm.contract.getProductPageList"));
    }

    /**
     * 根据id查询合同
     */
    public R queryById(Integer id) {
//        if (!authUtil.dataAuth("contract", "contract_id", id)) {
//            return R.ok().put("data", new Record().set("dataAuth", 0));
//        }
        Record record = Db.findFirst(Db.getSql("crm.contract.queryByContractId"), id);
        return R.ok().put("data", record);
    }

    /**
     * 根据id查询合同基本信息
     */
    public List<Record> information(Integer id) {
        Record record = Db.findFirst(Db.getSql("crm.contract.queryByContractId"), id);
        if (record == null) {
            return null;
        }
        List<Record> fieldList = new ArrayList<>();
        FieldUtil field = new FieldUtil(fieldList);
        field.set("合同编号", record.getStr("num"))
                .set("合同名称", record.getStr("name"))
                .set("客户名称", record.getStr("customer_name"))
                .set("下单时间", DateUtil.formatDate(record.getDate("order_date")))
                .set("合同金额", record.getStr("money"))
                .set("合同开始时间", DateUtil.formatDate(record.getDate("start_time")))
                .set("合同结束时间", DateUtil.formatDate(record.getDate("end_time")))
                .set("公司签约人", record.getStr("company_user_name"))
                .set("备注", record.getStr("remark"))
                .set("审核模板", record.getStr("examine_name"));
        List<Record> recordList = Db.find("select name,value from 72crm_admin_fieldv where batch_id = ?", record.getStr("batch_id"));
        Iterator<Record> iterator = recordList.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getStr("name").equals("附件")) {
                iterator.remove();
            }
        }
        fieldList.addAll(recordList);
        return fieldList;
    }

    /**
     * 根据id删除合同
     */
    @Before(Tx.class)
    public R deleteByIds(String contractIds) {

        String[] idsArr = contractIds.split(",");
        List<CrmReceivables> list = CrmReceivables.dao.find(Db.getSqlPara("crm.receivables.queryReceivablesByContractIds", Kv.by("contractIds", idsArr)));
        if (list.size() > 0) {
            return R.error("该数据已被其他模块引用，不能被删除！");
        }
        for (String id : idsArr) {
            CrmContract contract = CrmContract.dao.findById(id);
            if (contract.getCheckStatus() == 1 || contract.getCheckStatus() == 2) {
                return R.error("不能编辑，请先撤回再编辑！");
            }
            if (contract != null) {
                Db.delete("delete FROM 72crm_admin_fieldv where batch_id = ?", contract.getBatchId());
            }
            if (!CrmContract.dao.deleteById(id)) {
                return R.error();
            }
        }
        return R.ok();
    }

    /**
     * 添加或修改
     */
    @Before(Tx.class)
    public R saveAndUpdate(JSONObject jsonObject) {
        CrmContract crmContract = jsonObject.getObject("entity", CrmContract.class);
        String batchId = StrUtil.isNotEmpty(crmContract.getBatchId()) ? crmContract.getBatchId() : IdUtil.simpleUUID();
        crmRecordService.updateRecord(jsonObject.getJSONArray("field"), batchId);
        adminFieldService.save(jsonObject.getJSONArray("field"), batchId);
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        boolean flag;
        if (crmContract.getContractId() == null) {
            Integer contract = Db.queryInt(Db.getSql("crm.contract.queryByNum"), crmContract.getNum());
            if (contract != 0) {
                return R.error("合同编号已存在，请校对后再添加！");
            }
            crmContract.setCreateUserId(BaseUtil.getUser().getUserId().intValue());
            crmContract.setBatchId(batchId);
            crmContract.setCreateTime(DateUtil.date());
            crmContract.setUpdateTime(DateUtil.date());
            crmContract.setRoUserId(",");
            crmContract.setRwUserId(",");
            crmContract.setCheckStatus(0);//未提交
            crmContract.setOwnerUserId(BaseUtil.getUser().getUserId().intValue());

            crmContract.setNum("HT" + df.format(new Date()));//合同编号
//            Map<String, Integer> map = examineRecordService.saveExamineRecord(1, jsonObject.getLong("checkUserId"), crmContract.getOwnerUserId(), null);
//            if (map.get("status") == 0) {
//                return R.error("没有启动的审核步骤，不能添加！");
//            } else {
//                crmContract.setExamineRecordId(map.get("id"));
//            }
            flag = crmContract.save();
            crmRecordService.addRecord(crmContract.getContractId(), CrmEnum.CONTRACT_TYPE_KEY.getTypes());
        } else {
            CrmContract contract = CrmContract.dao.findById(crmContract.getContractId());
            if (contract.getCheckStatus() == 1 || contract.getCheckStatus() == 2) {
                return R.error("不能编辑，请先撤回再编辑！");
            }
//            Map<String, Integer> map = examineRecordService.saveExamineRecord(1, jsonObject.getLong("checkUserId"), contract.getOwnerUserId(), contract.getExamineRecordId());
//            if (map.get("status") == 0) {
//                return R.error("没有启动的审核步骤，不能添加！");
//            } else {
//                crmContract.setExamineRecordId(map.get("id"));
//            }
            crmContract.setCheckStatus(0);
            crmContract.setUpdateTime(DateUtil.date());
            crmRecordService.updateRecord(new CrmContract().dao().findById(crmContract.getContractId()), crmContract, CrmEnum.CONTRACT_TYPE_KEY.getTypes());
            flag = crmContract.update();
        }
//        JSONArray jsonArray = jsonObject.getJSONArray("product");
//        if (jsonArray != null) {
//            List<CrmContractProduct> contractProductList = jsonArray.toJavaList(CrmContractProduct.class);
//            //删除之前的合同产品关联表
//            Db.delete(Db.getSql("crm.contract.deleteByContractId"), crmContract.getContractId());
//            if (crmContract.getBusinessId() != null){
//                Db.delete("delete from 72crm_crm_business_product where business_id = ?",crmContract.getBusinessId());
//            }
//            if (contractProductList != null) {
//                for (CrmContractProduct crmContractProduct : contractProductList) {
//                    crmContractProduct.setContractId(crmContract.getContractId());
//                    crmContractProduct.save();
//                    if (crmContract.getBusinessId() != null){
//                        CrmBusinessProduct crmBusinessProduct = new CrmBusinessProduct()._setOrPut(crmContractProduct.toRecord().getColumns());
//                        crmBusinessProduct.setRId(null);
//                        crmBusinessProduct.setBusinessId(crmContract.getBusinessId());
//                        crmBusinessProduct.save();
//                    }
//                }
//            }
//
//        }

        return R.isSuccess(flag);

    }

    /**
     * 根据条件查询合同
     */
    public List<CrmContract> queryList(CrmContract crmContract) {
        StringBuffer sql = new StringBuffer("select * from 72crm_crm_contract where 1 = 1 ");
        if (crmContract.getCustomerId() != null) {
            sql.append(" and  customer_id = ").append(crmContract.getCustomerId());
        }
        if (crmContract.getBusinessId() != null) {
            sql.append(" and  business_id = ").append(crmContract.getBusinessId());
        }
        return CrmContract.dao.find(sql.toString());
    }

    /**
     * 根据条件查询合同
     */
    public List<Record> queryListByType(String type, Integer id) {
        StringBuffer sql = new StringBuffer("select * from contractview where ");
        if (type.equals(CrmEnum.CUSTOMER_TYPE_KEY.getTypes())) {
            sql.append("  customer_id = ? ");
        }
        if (type.equals(CrmEnum.BUSINESS_TYPE_KEY.getTypes())) {
            sql.append("  business_id = ? ");
        }

        return Db.find(sql.toString(), id);
    }

    /**
     * 根据合同批次查询产品
     *
     * @param batchId 合同批次
     * @return
     */
    public List<Record> queryProductById(String batchId) {
        return Db.find(Db.getSql("crm.contract.queryProductById"), batchId);
    }

    /**
     * 根据合同id查询回款
     *
     * @param id
     * @author HJP
     */
    public List<CrmReceivables> queryReceivablesById(Integer id) {
        return CrmReceivables.dao.find(Db.getSql("crm.receivables.queryReceivablesByContractId"), id);
    }

    /**
     * 根据合同id查询回款计划
     *
     * @param id
     * @author HJP
     */
    public List<CrmReceivablesPlan> queryReceivablesPlanById(Integer id) {
        return CrmReceivablesPlan.dao.find(Db.getSql("crm.receivablesplan.queryReceivablesPlanById"), id);
    }

    /**
     * 根据客户id变更负责人
     *
     * @author wyq
     */
    public R updateOwnerUserId(CrmCustomer crmCustomer) {
        CrmContract crmContract = new CrmContract();
        crmContract.setNewOwnerUserId(crmCustomer.getNewOwnerUserId());
        crmContract.setTransferType(crmCustomer.getTransferType());
        crmContract.setPower(crmCustomer.getPower());
        String contractIds = Db.queryStr("select GROUP_CONCAT(contract_id) from 72crm_crm_contract where customer_id in (" + crmCustomer.getCustomerIds() + ")");
        if (StrUtil.isEmpty(contractIds)) {
            return R.ok();
        }
        crmContract.setContractIds(contractIds);
        return transfer(crmContract);
    }

    /**
     * @author wyq
     * 根据合同id变更负责人
     */
    public R transfer(CrmContract crmContract) {
        String[] contractIdsArr = crmContract.getContractIds().split(",");
        return Db.tx(() -> {
            for (String contractId : contractIdsArr) {
                String memberId = "," + crmContract.getNewOwnerUserId() + ",";
                Db.update(Db.getSql("crm.contract.deleteMember"), memberId, memberId, Integer.valueOf(contractId));
                CrmContract oldContract = CrmContract.dao.findById(Integer.valueOf(contractId));
                if (2 == crmContract.getTransferType()) {
                    if (1 == crmContract.getPower()) {
                        crmContract.setRoUserId(oldContract.getRoUserId() + oldContract.getOwnerUserId() + ",");
                    }
                    if (2 == crmContract.getPower()) {
                        crmContract.setRwUserId(oldContract.getRwUserId() + oldContract.getOwnerUserId() + ",");
                    }
                }
                crmContract.setContractId(Integer.valueOf(contractId));
                crmContract.setOwnerUserId(crmContract.getNewOwnerUserId());
                crmContract.update();
                crmRecordService.addConversionRecord(Integer.valueOf(contractId), CrmEnum.CONTRACT_TYPE_KEY.getTypes(), crmContract.getNewOwnerUserId());
            }
            return true;
        }) ? R.ok() : R.error();
    }

    /**
     * @author wyq
     * 查询团队成员
     */
    public List<Record> getMembers(Integer contractId) {
        CrmContract crmContract = CrmContract.dao.findById(contractId);
        List<Record> recordList = new ArrayList<>();
        if (null != crmContract.getOwnerUserId()) {
            Record ownerUser = Db.findFirst(Db.getSql("crm.customer.getMembers"), crmContract.getOwnerUserId());
            recordList.add(ownerUser.set("power", "负责人权限").set("groupRole", "负责人"));
        }
        String roUserId = crmContract.getRoUserId();
        String rwUserId = crmContract.getRwUserId();
        String memberIds = roUserId + rwUserId.substring(1);
        if (",".equals(memberIds)) {
            return recordList;
        }
        String[] memberIdsArr = memberIds.substring(1, memberIds.length() - 1).split(",");
        Set<String> memberIdsSet = new HashSet<>(Arrays.asList(memberIdsArr));
        for (String memberId : memberIdsSet) {
            Record record = Db.findFirst(Db.getSql("crm.customer.getMembers"), memberId);
            if (roUserId.contains(memberId)) {
                record.set("power", "只读").set("groupRole", "普通成员");
            }
            if (rwUserId.contains(memberId)) {
                record.set("power", "读写").set("groupRole", "普通成员");
            }
            recordList.add(record);
        }
        return recordList;
    }

    /**
     * @author wyq
     * 添加团队成员
     */
    @Before(Tx.class)
    public R addMember(CrmContract crmContract) {
        String[] contractIdsArr = crmContract.getIds().split(",");
        String[] memberArr = crmContract.getMemberIds().split(",");
        StringBuilder stringBuilder = new StringBuilder();
        for (String id : contractIdsArr) {
            Integer ownerUserId = CrmContract.dao.findById(Integer.valueOf(id)).getOwnerUserId();
            for (String memberId : memberArr) {
                if (ownerUserId.equals(Integer.valueOf(memberId))) {
                    return R.error("负责人不能重复选为团队成员");
                }
                Db.update(Db.getSql("crm.contract.deleteMember"), "," + memberId + ",", "," + memberId + ",", Integer.valueOf(id));
            }
            if (1 == crmContract.getPower()) {
                stringBuilder.setLength(0);
                String roUserId = stringBuilder.append(CrmContract.dao.findById(Integer.valueOf(id)).getRoUserId()).append(crmContract.getMemberIds()).append(",").toString();
                Db.update("update 72crm_crm_contract set ro_user_id = ? where contract_id = ?", roUserId, Integer.valueOf(id));
            }
            if (2 == crmContract.getPower()) {
                stringBuilder.setLength(0);
                String rwUserId = stringBuilder.append(CrmContract.dao.findById(Integer.valueOf(id)).getRwUserId()).append(crmContract.getMemberIds()).append(",").toString();
                Db.update("update 72crm_crm_contract set rw_user_id = ? where contract_id = ?", rwUserId, Integer.valueOf(id));
            }
        }
        return R.ok();
    }

    /**
     * @author wyq
     * 删除团队成员
     */
    public R deleteMembers(CrmContract crmContract) {
        String[] contractIdsArr = crmContract.getIds().split(",");
        String[] memberArr = crmContract.getMemberIds().split(",");
        return Db.tx(() -> {
            for (String id : contractIdsArr) {
                for (String memberId : memberArr) {
                    Db.update(Db.getSql("crm.contract.deleteMember"), "," + memberId + ",", "," + memberId + ",", Integer.valueOf(id));
                }
            }
            return true;
        }) ? R.ok() : R.error();
    }


    /**
     * @author zxy
     * 查询合同自定义字段（添加）
     */
    public List<Record> queryField() {
        List<Record> fieldList = new ArrayList<>();
        String[] settingArr = new String[]{};
        fieldUtil.getFixedField(fieldList, "num", "合同编号", "", "number", settingArr, 1);
        fieldUtil.getFixedField(fieldList, "name", "合同名称", "", "text", settingArr, 1);
        fieldUtil.getFixedField(fieldList, "customerId", "客户名称", settingArr, "customer", settingArr, 1);
        fieldUtil.getFixedField(fieldList, "businessId", "商机名称", settingArr, "business", settingArr, 0);
        fieldUtil.getFixedField(fieldList, "orderDate", "下单时间", "", "date", settingArr, 0);
        fieldUtil.getFixedField(fieldList, "money", "合同金额", "", "floatnumber", settingArr, 1);
        fieldUtil.getFixedField(fieldList, "startTime", "合同开始时间", "", "date", settingArr, 0);
        fieldUtil.getFixedField(fieldList, "endTime", "合同结束时间", "", "date", settingArr, 0);
        fieldUtil.getFixedField(fieldList, "contactsId", "客户签约人", settingArr, "contacts", settingArr, 0);
        fieldUtil.getFixedField(fieldList, "companyUserId", "公司签约人", settingArr, "user", settingArr, 0);
        fieldUtil.getFixedField(fieldList, "remark", "备注", "", "textarea", settingArr, 0);
        fieldUtil.getFixedField(fieldList, "product", "产品", Kv.by("discount_rate", "").set("product", new ArrayList<>()).set("total_price", ""), "product", settingArr, 0);
        fieldList.addAll(adminFieldService.list("6"));
        return fieldList;
    }

    /**
     * @author wyq
     * 查询编辑字段
     */
    public List<Record> queryField(Integer contractId) {
        Record contract = Db.findFirst("select * from contractview1 where contract_id = ?", contractId);
        List<Record> list = new ArrayList<>();
        list.add(new Record().
                set("customer_id", contract.getInt("customer_id")).
                set("customer_name", contract.getStr("customer_name")));
        contract.set("customer_id", list);

        list = new ArrayList<>();
        list.add(new Record().
                set("owner_user_id", contract.getInt("owner_user_id")).set("realname", contract.getStr("owner_user_name")));
        contract.set("owner_user_id", list);

        list = new ArrayList<>();
        list.add(new Record().
                set("company_user_id", contract.getInt("company_user_id")).set("realname", contract.getStr("company_user_name")));
        contract.set("company_user_id", list);

        list = new ArrayList<>();
        list.add(new Record().
                set("examine_id", contract.getInt("examine_id")).set("examine_name", contract.getStr("examine_name")));
        contract.set("examine_id", list);

        list = new ArrayList<>();
        if (contract.getStr("business_id") != null && contract.getInt("business_id") != 0) {
            list.add(new Record()
                    .set("business_id", contract.getInt("business_id")).set("business_name", contract.getStr("business_name")));
        }
        contract.set("business_id", list);
        list = new ArrayList<>();
        if (contract.getStr("contacts_id") != null && contract.getInt("contacts_id") != 0) {
            list.add(new Record().set("contacts_id", contract.getStr("contacts_id")).set("name", contract.getStr("contacts_name")));
        }
        contract.set("contacts_id", list);
//        list = new ArrayList<>();
//        if (contract.getStr("company_user_id") != null && contract.getInt("company_user_id") != 0) {
//            list.add(new Record().set("company_user_id", contract.getStr("company_user_id")).set("realname", contract.getStr("company_user_name")));
//        }
//        contract.set("company_user_id",list);
        List<Record> fieldList = adminFieldService.queryUpdateField(6, contract);
        Record totalPrice = Db.findFirst("select IFNULL(SUM(subtotal),0) as total_price from 72crm_crm_contract_product where contract_id = ? ", contractId);
        Kv kv = Kv.by("discount_rate", contract.getBigDecimal("discount_rate"))
                .set("product", Db.find(Db.getSql("crm.contract.queryBusinessProduct"), contractId))
                .set("total_price", totalPrice.getStr("total_price"));
        fieldList.add(new Record().set("field_name", "product").set("value", kv).set("setting", new String[]{}).set("is_null", 0).set("field_type", 1));
        return fieldList;
    }

    /**
     * @author zxy
     * 查询合同自定义字段（编辑）
     */
//    public List<Record> queryField(Integer contractId) {
//        List<Record> fieldList = new ArrayList<>();
//        Record record = Db.findFirst("select * from contractview where contract_id = ?", contractId);
//        String[] settingArr = new String[]{};
//        fieldUtil.getFixedField(fieldList, "num", "合同编号", record.getStr("num"), "number", settingArr, 1);
//        fieldUtil.getFixedField(fieldList, "name", "合同名称", record.getStr("name"), "text", settingArr, 1);
//        List<Record> customerList = new ArrayList<>();
//        Record customer = new Record();
//        customerList.add(customer.set("customerId", record.getInt("customer_id")).set("customerName", record.getStr("customer_name")));
//        fieldUtil.getFixedField(fieldList, "customerId", "客户名称", customerList, "customer", settingArr, 1);
//        customerList = new ArrayList<>();
//        if (record.getStr("business_id") != null && record.getInt("business_id") != 0) {
//            customer = new Record();
//            customerList.add(customer.set("businessId", record.getInt("business_id")).set("businessName", record.getStr("business_name")));
//        }
//
//        fieldUtil.getFixedField(fieldList, "businessId", "商机名称", customerList, "business", settingArr, 0);
//        fieldUtil.getFixedField(fieldList, "orderDate", "下单时间", DateUtil.formatDateTime(record.get("order_date")), "date", settingArr, 0);
//        fieldUtil.getFixedField(fieldList, "money", "合同金额", record.getStr("money"), "floatnumber", settingArr, 1);
//        fieldUtil.getFixedField(fieldList, "startTime", "合同开始时间", DateUtil.formatDateTime(record.get("start_time")), "date", settingArr, 0);
//        fieldUtil.getFixedField(fieldList, "endTime", "合同结束时间", DateUtil.formatDateTime(record.get("end_time")), "date", settingArr, 0);
//        customerList = new ArrayList<>();
//        if (record.getStr("contacts_id") != null && record.getInt("contacts_id") != 0) {
//            customer = new Record();
//            customerList.add(customer.set("contactsId", record.getStr("contacts_id")).set("name", record.getStr("contacts_name")));
//        }
//        fieldUtil.getFixedField(fieldList, "contactsId", "客户签约人", customerList, "contacts", settingArr, 0);
//        customerList = new ArrayList<>();
//        if (record.getStr("company_user_id") != null && record.getInt("company_user_id") != 0) {
//            customer = new Record();
//            customerList.add(customer.set("companyUserId", record.getStr("company_user_id")).set("realname", record.getStr("company_user_name")));
//        }
//        fieldUtil.getFixedField(fieldList, "companyUserId", "公司签约人", customerList, "user", settingArr, 0);
//        fieldUtil.getFixedField(fieldList, "remark", "备注", record.getStr("remark"), "textarea", settingArr, 0);
//        fieldList.addAll(adminFieldService.queryByBatchId(record.getStr("batch_id")));
//        Record r = Db.findFirst("select IFNULL(SUM(subtotal),0) as total_price \n" +
//                "    from 72crm_crm_contract_product    \n" +
//                "    where contract_id = ? ", contractId);
//
//        Kv kv = Kv.by("discount_rate", record.getBigDecimal("discount_rate"))
//                .set("product", Db.find(Db.getSql("crm.contract.queryBusinessProduct"), contractId))
//                .set("total_price", r.getStr("total_price"));
//
//        fieldUtil.getFixedField(fieldList, "product", "产品", kv, "product", settingArr, 0);
//        return fieldList;
//    }

    /**
     * @author wyq
     * 添加跟进记录
     */
    @Before(Tx.class)
    public R addRecord(AdminRecord adminRecord) {
        adminRecord.setTypes("crm_contract");
        adminRecord.setCreateTime(DateUtil.date());
        adminRecord.setCreateUserId(BaseUtil.getUser().getUserId().intValue());
        if (1 == adminRecord.getIsEvent()) {
            OaEvent oaEvent = new OaEvent();
            oaEvent.setTitle(adminRecord.getContent());
            oaEvent.setStartTime(adminRecord.getNextTime());
            oaEvent.setEndTime(DateUtil.offsetDay(adminRecord.getNextTime(), 1));
            oaEvent.setCreateTime(DateUtil.date());
            oaEvent.setCreateUserId(BaseUtil.getUser().getUserId().intValue());
            oaEvent.save();

            AdminUser user = BaseUtil.getUser();
            oaActionRecordService.addRecord(oaEvent.getEventId(), OaEnum.EVENT_TYPE_KEY.getTypes(), 1, oaActionRecordService.getJoinIds(user.getUserId().intValue(), oaEvent.getOwnerUserIds()), oaActionRecordService.getJoinIds(user.getDeptId(), ""));
            OaEventRelation oaEventRelation = new OaEventRelation();
            oaEventRelation.setEventId(oaEvent.getEventId());
            oaEventRelation.setContractIds("," + adminRecord.getTypesId().toString() + ",");
            oaEventRelation.setCreateTime(DateUtil.date());
            oaEventRelation.save();
        }
        return adminRecord.save() ? R.ok() : R.error();
    }

    /**
     * @author wyq
     * 查看跟进记录
     */
    public List<Record> getRecord(BasePageRequest<CrmContract> basePageRequest) {
        CrmContract crmContract = basePageRequest.getData();
        List<Record> recordList = Db.find(Db.getSql("crm.contract.getRecord"), crmContract.getContractId());
        recordList.forEach(record -> {
            adminFileService.queryByBatchId(record.getStr("batch_id"), record);
        });
        return recordList;
    }

    /**
     * 根据合同ID查询产品
     */
    public R qureyProductListByContractId(BasePageRequest<CrmContractProduct> basePageRequest) {

        Integer pageType = basePageRequest.getPageType();
        Record record = Db.findFirst(Db.getSql("crm.product.querySubtotalByContractId"), basePageRequest.getData().getContractId());
        if (record.getStr("money") == null) {
            record.set("money", 0);
        }
        if (0 == pageType) {
            record.set("list", Db.find(Db.getSql("crm.product.queryProductPageList"), basePageRequest.getData().getContractId()));
            return R.ok().put("data", record);
        } else {
            Page<Record> page = Db.paginateByFullSql(basePageRequest.getPage(), basePageRequest.getLimit(), Db.getSql("crm.product.queryProductPagecount"), Db.getSql("crm.product.queryProductPageList"), basePageRequest.getData().getContractId());
            record.set("pageNumber", page.getPageNumber());
            record.set("pageSize", page.getPageSize());
            record.set("totalPage", page.getTotalPage());
            record.set("totalRow", page.getTotalRow());
            record.set("list", page.getList());
            return R.ok().put("data", record);
        }
    }

    /**
     * 查询合同到期提醒设置
     */
    public R queryContractConfig() {
        Record config = Db.findFirst("select status,value as contractDay from 72crm_admin_config where name = 'expiringContractDays'");
        if (config == null) {
            AdminConfig adminConfig = new AdminConfig();
            adminConfig.setStatus(0);
            adminConfig.setName("expiringContractDays");
            adminConfig.setValue("3");
            adminConfig.setDescription("合同到期提醒");
            adminConfig.save();
            config.set("status", 0).set("value", "3");
        }
        return R.ok().put("data", config);
    }

    /**
     * 修改合同到期提醒设置
     */
    @Before(Tx.class)
    public R setContractConfig(Integer status, Integer contractDay) {
        if (status == 1 && contractDay == null) {
            return R.error("contractDay不能为空");
        }
        Integer number = Db.update(Db.getSqlPara("crm.contract.setContractConfig", Kv.by("status", status).set("contractDay", contractDay)));
        if (0 == number) {
            AdminConfig adminConfig = new AdminConfig();
            adminConfig.setStatus(0);
            adminConfig.setName("expiringContractDays");
            adminConfig.setValue("3");
            adminConfig.setDescription("合同到期提醒");
            adminConfig.save();
        }
        return R.ok();
    }

    //提交审核
    @Before(Tx.class)
    public R auditContract(Integer contractId) {
        Map<String, Integer> map = new HashMap<>();
        //创建审核记录
        AdminExamineRecord examineRecord = new AdminExamineRecord();
        examineRecord.setCreateTime(DateUtil.date());
        examineRecord.setCreateUser(BaseUtil.getUser().getUserId());
        CrmContract contract = CrmContract.dao.findById(contractId);

        //创建审核日志
        AdminExamineLog examineLog = new AdminExamineLog();
        examineRecord.setExamineStatus(3);
        examineLog.setCreateTime(DateUtil.date());
        examineLog.setCreateUser(BaseUtil.getUser().getUserId());
        examineLog.setExamineStatus(0);//审核中
        examineLog.setOrderId(1);
        //根据模板id查询当前审批流程
        AdminExamine examine = AdminExamine.dao.findFirst(Db.getSql("admin.examine.getExamineById"), contract.getExamineId());
        if (examine == null) {
            map.put("status", 0);
        } else {
            examineRecord.setExamineId(examine.getExamineId());
            //固定审批
            //先查询该审批流程的审批步骤的第一步
            AdminExamineStep examineStep = AdminExamineStep.dao.findFirst(Db.getSql("admin.examineStep.queryExamineStepByExamineIdOrderByStepNum"), examine.getExamineId());
            examineRecord.setExamineStepId(examineStep.getStepId());//当前进行审批的步骤
            examineLog.setExamineStepId(examineStep.getStepId());
            examineRecord.save();
            contract.setExamineRecordId(examineRecord.getRecordId());
            //判断步骤类型 指定用户
            if (examineStep.getStepType() == 2 || examineStep.getStepType() == 3) {
                String[] userIds = examineStep.getCheckUserId().split(",");
                for (String id : userIds) {
                    if (StrUtil.isNotEmpty(id)) {
                        examineLog.setLogId(null);
                        examineLog.setExamineUser(Long.valueOf(id));
                        examineLog.setRecordId(examineRecord.getRecordId());
                        examineLog.setIsRecheck(0);
                        examineLog.save();
                    }
                }
            } else if (examineStep.getStepType() == 1) {
                //如果是负责人主管审批 获取主管ID
                Record r = Db.findFirst(Db.getSql("admin.examineLog.queryUserByUserId"), contract.getOwnerUserId());
                if (r == null || r.getLong("user_id") == null) {
                    examineLog.setExamineUser(BaseConstant.SUPER_ADMIN_USER_ID);
                } else {
                    examineLog.setExamineUser(r.getLong("user_id"));
                }
                examineLog.setRecordId(examineRecord.getRecordId());
                examineLog.setIsRecheck(0);
                examineLog.save();
            } else {
                //如果是负责人主管审批 获取主管的主管ID
                Record r = Db.findFirst(Db.getSql("admin.examineLog.queryUserByUserId"), Db.findFirst(Db.getSql("admin.examineLog.queryUserByUserId"), contract.getOwnerUserId()).getLong("user_id"));
                if (r == null || r.getLong("user_id") == null) {
                    examineLog.setExamineUser(Long.valueOf(BaseConstant.SUPER_ADMIN_USER_ID));
                } else {
                    examineLog.setExamineUser(r.getLong("user_id"));
                }
                examineLog.setRecordId(examineRecord.getRecordId());
                examineLog.setIsRecheck(0);
                examineLog.save();
            }
            map.put("status", 1);
            map.put("id", examineRecord.getRecordId());
            contract.setExamineId(examineRecord.getExamineId());
            contract.setCheckStatus(1);
            contract.setUpdateTime(DateUtil.date());
            contract.update();
        }
        return R.ok();
    }


    /**
     * 审核合同或者回款 recordId:审核记录id status:审批状态：审核状态  1 审核通过 2 审核拒绝 4 已撤回
     * remarks:审核备注 id:审核对象的id（合同或者回款的id）nextUserId:下一个审批人 ownerUserId:负责人
     */
    @Before(Tx.class)
    public R auditExamine(Integer recordId, Integer status, String remarks, Integer id, Long ownerUserId) {

        //当前审批人
        Long auditUserId = BaseUtil.getUser().getUserId();

        //根据审核记录id查询审核记录
        AdminExamineRecord examineRecord = AdminExamineRecord.dao.findById(recordId);
        if (status == 4) {
            //只有创建人或者超级admin才有撤回的权限
            if (!examineRecord.getCreateUser().equals(auditUserId) && !auditUserId.equals(BaseConstant.SUPER_ADMIN_USER_ID)) {
                return R.error("当前用户没有审批权限！");
            }
        } else {
            //【判断当前审批人是否有审批权限
            Record reco = Db.findFirst(Db.getSqlPara("admin.examineLog.queryExamineLog",
                    Kv.by("recordId", recordId).set("auditUserId", auditUserId).set("stepId", examineRecord.getExamineStepId())));
            if (reco == null) {
                return R.error("当前用户没有审批权限！");
            }
        }
        examineRecord.setExamineStatus(status);
        //查询审批流程
        AdminExamine examine = AdminExamine.dao.findById(examineRecord.getExamineId());
        if (examine.getCategoryType() == 1) {//合同
            ownerUserId = Long.valueOf(CrmContract.dao.findById(id).getOwnerUserId());
        }
        //查询当前审批步骤
        AdminExamineStep examineStep = AdminExamineStep.dao.findById(examineRecord.getExamineStepId());
        //查询当前审核日志
        AdminExamineLog nowadayExamineLog = null;
        if (examine.getExamineType() == 1) {//固定审核流程
            nowadayExamineLog = AdminExamineLog.dao.findFirst(Db.getSql("admin.examineLog.queryNowadayExamineLogByRecordIdAndStepId"), examineRecord.getRecordId(), examineRecord.getExamineStepId(), auditUserId);
        } else {
            nowadayExamineLog = AdminExamineLog.dao.findFirst(Db.getSql("admin.examineLog.queryNowadayExamineLogByRecordIdAndStatus"), examineRecord.getRecordId(), auditUserId);
        }

        //审核日志 添加审核人
        if (nowadayExamineLog != null) {
            nowadayExamineLog.setExamineTime(DateUtil.date());
            nowadayExamineLog.setRemarks(remarks);
        }

        if (status == 2) {
            //判断审核拒绝
            nowadayExamineLog.setExamineStatus(status);
            if (examineStep != null && examineStep.getStepType() == 2) {//指定用户
                examineRecord.setExamineStatus(3);
                Record record = Db.findFirst(Db.getSqlPara("admin.examineLog.queryCountByStepId", Kv.by("recordId", recordId).set("stepId", examineStep.getStepId())));
                if (record.getInt("toCount") == 0) {
                    examineRecord.setExamineStatus(status);
                }
            }

            if (examine.getCategoryType() == 1) {
                //合同
                Db.update(Db.getSql("crm.contract.updateCheckStatusById"), 3, id);
            } else {
                //回款
                Db.update(Db.getSql("crm.receivables.updateCheckStatusById"), 3, id);
            }
        } else if (status == 4) {
            //先查询该审批流程的审批步骤的第一步
            AdminExamineStep oneExamineStep = AdminExamineStep.dao.findFirst(Db.getSql("admin.examineStep.queryExamineStepByExamineIdOrderByStepNum"), examine.getExamineId());
            //判断审核撤回
            AdminExamineLog examineLog = new AdminExamineLog();
            examineLog.setLogId(null);
            examineLog.setExamineUser(Long.valueOf(auditUserId));
            examineLog.setCreateTime(DateUtil.date());
            examineLog.setCreateUser(auditUserId);
            examineLog.setExamineStatus(status);
            examineLog.setIsRecheck(0);
            if (examine.getExamineType() == 1) {//固定
                examineRecord.setExamineStepId(oneExamineStep.getStepId());
                examineLog.setExamineStepId(examineStep.getStepId());
                examineLog.setOrderId(examineStep.getStepNum());
            }
            examineLog.setRecordId(examineRecord.getRecordId());
            examineLog.setRemarks(remarks);
            examineLog.save();
            if (examine.getCategoryType() == 1) {
                //合同
                CrmContract contract = CrmContract.dao.findById(id);
                if (contract.getCheckStatus() == 2) {
                    return R.error("该合同已审核通过，不能撤回！");
                }
                Db.update(Db.getSql("crm.contract.updateCheckStatusById"), 4, id);
            } else {
                //回款
                CrmReceivables receivables = CrmReceivables.dao.findById(id);
                if (receivables.getCheckStatus() == 2) {
                    return R.error("该回款已审核通过，不能撤回！");
                }
                Db.update(Db.getSql("crm.receivables.updateCheckStatusById"), 4, id);
            }
        } else {
            //审核通过
            nowadayExamineLog.setExamineStatus(status);
            //判断该审批流程类型
            if (examine.getExamineType() == 1) {
                //固定审批

                //查询下一个审批步骤
                AdminExamineStep nextExamineStep =
                        AdminExamineStep.dao.findFirst(Db.getSql("admin.examineStep.queryExamineStepByNextExamineIdOrderByStepId"), examine.getExamineId(), examineRecord.getExamineStepId());

                Boolean flag = true;
                //判断是否是并签
                if (examineStep.getStepType() == 3) {
                    //查询当前并签是否都完成
                    //根据审核记录ID，审核步骤ID，查询审核日志
                    // List<AdminExamineLog> examineLogs = AdminExamineLog.dao.find(Db.getSql("admin.examineLog.queryNowadayExamineLogByRecordIdAndStepId"),examineRecord.getRecordId(),examineRecord.getExamineStepId());
                    //当前并签人员
                    nowadayExamineLog.update();
                    String[] userIds = examineStep.getCheckUserId().split(",");
                    for (String userId : userIds) {
                        if (StrUtil.isNotEmpty(userId)) {
                            AdminExamineLog examineLog = AdminExamineLog.dao.findFirst(Db.getSql("admin.examineLog.queryNowadayExamineLogByRecordIdAndStepId"), examineRecord.getRecordId(), examineRecord.getExamineStepId(), userId);
                            if (examineLog.getExamineStatus() == 0) {
                                //并签未走完
                                flag = false;
                                break;
                            }
                        }
                    }
                    //并签未完成
                    if (!flag) {
                        examineRecord.setExamineStatus(3);
                        if (examine.getCategoryType() == 1) {
                            //合同
                            Db.update(Db.getSql("crm.contract.updateCheckStatusById"), 1, id);

                        } else {
                            //回款
                            Db.update(Db.getSql("crm.receivables.updateCheckStatusById"), 1, id);

                        }
                    }
                }
                if (flag) {
                    //判断是否有下一步流程
                    if (nextExamineStep != null) {
                        //有下一步流程
                        examineRecord.setExamineStatus(3);
                        examineRecord.setExamineStepId(nextExamineStep.getStepId());

                        AdminExamineLog examineLog = new AdminExamineLog();
                        examineLog.setOrderId(nextExamineStep.getStepNum());
                        if (nextExamineStep.getStepType() == 2 || nextExamineStep.getStepType() == 3) {
                            //并签或者或签
                            String[] userIds = nextExamineStep.getCheckUserId().split(",");
                            for (String uid : userIds) {
                                if (StrUtil.isNotEmpty(uid)) {
                                    examineLog.setLogId(null);
                                    examineLog.setExamineUser(Long.valueOf(uid));
                                    examineLog.setCreateTime(DateUtil.date());
                                    examineLog.setCreateUser(BaseUtil.getUser().getUserId());
                                    examineLog.setExamineStatus(0);
                                    examineLog.setIsRecheck(0);
                                    examineLog.setExamineStepId(nextExamineStep.getStepId());
                                    examineLog.setRecordId(examineRecord.getRecordId());

                                    examineLog.save();
                                }
                            }
                        } else if (nextExamineStep.getStepType() == 1) {
                            Record r = Db.findFirst(Db.getSql("admin.examineLog.queryUserByUserId"), ownerUserId);
                            examineLog.setLogId(null);
                            if (r == null || r.getLong("user_id") == null) {
                                examineLog.setExamineUser(BaseConstant.SUPER_ADMIN_USER_ID);
                            } else {
                                examineLog.setExamineUser(r.getLong("user_id"));

                            }
                            examineLog.setExamineStatus(0);
                            examineLog.setCreateTime(DateUtil.date());
                            examineLog.setCreateUser(BaseUtil.getUser().getUserId());
                            examineLog.setIsRecheck(0);
                            examineLog.setExamineStepId(nextExamineStep.getStepId());
                            examineLog.setRecordId(examineRecord.getRecordId());
                            examineLog.save();
                        } else {
                            Record r = Db.findFirst(Db.getSql("admin.examineLog.queryUserByUserId"), Db.findFirst(Db.getSql("admin.examineLog.queryUserByUserId"), ownerUserId).getLong("user_id"));
                            examineLog.setLogId(null);
                            if (r == null || r.getLong("user_id") == null) {
                                examineLog.setExamineUser(BaseConstant.SUPER_ADMIN_USER_ID);
                            } else {
                                examineLog.setExamineUser(r.getLong("user_id"));

                            }
                            examineLog.setExamineStatus(0);
                            examineLog.setCreateTime(DateUtil.date());
                            examineLog.setCreateUser(BaseUtil.getUser().getUserId());
                            examineLog.setExamineStepId(nextExamineStep.getStepId());
                            examineLog.setRecordId(examineRecord.getRecordId());
                            examineLog.setIsRecheck(0);
                            examineLog.save();
                        }

                        // AdminExamineLog examineLog = new AdminExamineLog();
                        if (examine.getCategoryType() == 1) {
                            //合同
                            Db.update(Db.getSql("crm.contract.updateCheckStatusById"), 1, id);
                        } else {
                            //回款
                            Db.update(Db.getSql("crm.receivables.updateCheckStatusById"), 1, id);
                        }
                    } else {
                        //没有下一审批流程步骤
                        if (examine.getCategoryType() == 1) {
                            //合同
                            Db.update(Db.getSql("crm.contract.updateCheckStatusById"), 2, id);
                            CrmContract contract = CrmContract.dao.findById(id);
                            Db.update(Db.getSql("crm.customer.updateDealStatusById"), "已成交", contract.getCustomerId());
                        } else {
                            //回款
                            Db.update(Db.getSql("crm.receivables.updateCheckStatusById"), 2, id);
                        }

                    }
                }
            }
        }
        if (status != 4) {
            nowadayExamineLog.update();
        }
        return examineRecord.update() ? R.ok() : R.error();
    }

}

