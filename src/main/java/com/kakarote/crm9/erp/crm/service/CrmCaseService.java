package com.kakarote.crm9.erp.crm.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.kakarote.crm9.common.config.paragetter.BasePageRequest;
import com.kakarote.crm9.common.constant.BaseConstant;
import com.kakarote.crm9.erp.admin.entity.*;
import com.kakarote.crm9.erp.admin.service.AdminFieldService;
import com.kakarote.crm9.erp.admin.service.AdminFileService;
import com.kakarote.crm9.erp.admin.service.AdminSceneService;
import com.kakarote.crm9.erp.crm.common.CrmEnum;
import com.kakarote.crm9.erp.crm.common.CrmParamValid;
import com.kakarote.crm9.erp.crm.entity.*;
import com.kakarote.crm9.erp.oa.common.OaEnum;
import com.kakarote.crm9.erp.oa.entity.OaEvent;
import com.kakarote.crm9.erp.oa.entity.OaEventRelation;
import com.kakarote.crm9.erp.oa.service.OaActionRecordService;
import com.kakarote.crm9.utils.AuthUtil;
import com.kakarote.crm9.utils.BaseUtil;
import com.kakarote.crm9.utils.FieldUtil;
import com.kakarote.crm9.utils.R;

import java.text.SimpleDateFormat;
import java.util.*;

public class CrmCaseService {
    @Inject
    private AdminFieldService adminFieldService;

    @Inject
    private FieldUtil fieldUtil;

    @Inject
    private CrmRecordService crmRecordService;

    @Inject
    private AdminFileService adminFileService;

    @Inject
    private AdminSceneService adminSceneService;

    @Inject
    private OaActionRecordService oaActionRecordService;

    @Inject
    private CrmParamValid crmParamValid;

    @Inject
    private AuthUtil authUtil;

    public R addOrUpdate(JSONObject jsonObject, String type) {
        CrmCase crmCase = jsonObject.getObject("entity", CrmCase.class);
        String batchId = StrUtil.isNotEmpty(crmCase.getBatchId()) ? crmCase.getBatchId() : IdUtil.simpleUUID();
        crmRecordService.updateRecord(jsonObject.getJSONArray("field"), batchId);
        adminFieldService.save(jsonObject.getJSONArray("field"), batchId);
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        //修改
        if (crmCase.getCaseId() != null) {
            CrmCase oldCrmCase = new CrmCase().dao().findById(crmCase.getCaseId());
            crmRecordService.updateRecord(oldCrmCase, crmCase, CrmEnum.CASE_TYPE_KEY.getTypes());
            crmCase.setUpdateTime(DateUtil.date());
            return crmCase.update() ? R.ok() : R.error();
        } else {
            crmCase.setCreateTime(DateUtil.date());
            crmCase.setUpdateTime(DateUtil.date());
            crmCase.setCreateUserId(BaseUtil.getUser().getUserId().intValue());
            crmCase.setBatchId(batchId);
            crmCase.setNum("AJ" + df.format(new Date()));
            crmCase.setRwUserId(",");
            crmCase.setRoUserId(",");
            boolean save = crmCase.save();
            crmRecordService.addRecord(crmCase.getCaseId(), CrmEnum.CASE_TYPE_KEY.getTypes());
            return save ? R.ok().put("data", Kv.by("case_id", crmCase.getCaseId())) : R.error();
        }
    }

    public R queryById(Integer id) {
        if (!authUtil.dataAuth("case", "case_id", id)) {
            return R.ok().put("data", new Record().set("dataAuth", 0));
        }
        Record record = Db.findFirst(Db.getSql("crm.case.queryByCaseId"), id);
        return R.ok().put("data", record);
    }

    @Before(Tx.class)
    public R deleteByIds(String caseIds) {
        String[] idsArr = caseIds.split(",");
        for (String id : idsArr) {
            CrmCase crmCase = CrmCase.dao.findById(id);
            if (crmCase != null) {
                Db.delete("delete FROM 72crm_admin_fieldv where batch_id = ?", crmCase.getBatchId());
            }
            if (!CrmCase.dao.deleteById(id)) {
                return R.error();
            }
        }
        return R.ok();
    }

    /**
     * @author wyq
     * 查询回款自定义字段（编辑）
     */
    public List<Record> queryField(Integer caseId) {
        Record casemodule = Db.findFirst("SELECT * FROM caseview WHERE case_id  = ?", caseId);
        List<Record> customerList = new ArrayList<>();
        Record customer = new Record();
        customerList.add(customer.set("customer_id", casemodule.getInt("customer_id")).set("customer_name", casemodule.getStr("customer_name")));
        casemodule.set("customer_id", customerList);

        List<Record> userList = new ArrayList<>();
        Record user = new Record();
        userList.add(user.set("user_id", casemodule.getInt("owner_user_id")).set("realname", casemodule.getStr("owner_user_name")));
        casemodule.set("owner_user_id", userList);

        List<Record> businessList = new ArrayList<>();
        Record business = new Record();
        businessList.add(business.set("business_id", casemodule.getInt("business_id")).set("business_name", casemodule.getStr("business_name")));
        casemodule.set("business_id", businessList);

        List<Record> contractList = new ArrayList<>();
        Record contract = new Record();
        contractList.add(contract.set("contract_id", casemodule.getInt("contract_id")).set("contract_num", casemodule.getStr("contract_num")));
        casemodule.set("contract_id", contractList);

        List<Object> examineList = new ArrayList<>();
        Record record = new Record();
        examineList.add(record.set("examine_id", casemodule.getInt("examine_id")).set("examine_name", casemodule.getStr("examine_name")));
        casemodule.set("examine_id", examineList);

        List<Record> fieldList = adminFieldService.queryUpdateField(9, casemodule);
        return fieldList;
    }

    /**
     * 根据id查询案件基本信息
     */
    public List<Record> information(Integer id) {
        Record record = Db.findFirst(Db.getSql("crm.case.queryByCaseId"), id);
        if (record == null) {
            return null;
        }
        List<Record> fieldList = new ArrayList<>();
        FieldUtil field = new FieldUtil(fieldList);
        field.set("案件标题", record.getStr("name"))
                .set("关联客户", record.getStr("customer_name"))
                .set("主要负责人", record.getStr("owner_user_name"))
                .set("案件总金额", record.getStr("money"))
                .set("合同名称", record.getStr("contract_name"))
                .set("关联商机", record.getStr("business_name"))
                .set("下次跟进时间", DateUtil.formatDate(record.getDate("next_time")))
                .set("案件编号", record.getStr("num"))
                .set("备注", record.getStr("remark"));
        List<Record> recordList = Db.find("select name,value from 72crm_admin_fieldv where batch_id = ?", record.getStr("batch_id"));
        Iterator<Record> iterator = recordList.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getStr("name").equals("附件")) {
                iterator.remove();
            }
        }
        fieldList.addAll(recordList);
        return fieldList;
    }


    /**
     * @author wyq
     * 添加跟进记录
     */
    @Before(Tx.class)
    public R addRecord(AdminRecord adminRecord) {
        adminRecord.setTypes("crm_case");
        adminRecord.setCreateTime(DateUtil.date());
        adminRecord.setCreateUserId(BaseUtil.getUser().getUserId().intValue());
        return adminRecord.save() ? R.ok() : R.error();
    }

    /**
     * @author wyq
     * 查看跟进记录
     */
    public List<Record> getRecord(BasePageRequest<CrmCase> basePageRequest) {
        CrmCase crmCase = basePageRequest.getData();
        List<Record> recordList = Db.find(Db.getSql("crm.case.getRecord"), crmCase.getCaseId());
        recordList.forEach(record -> {
            adminFileService.queryByBatchId(record.getStr("batch_id"), record);
            String businessIds = record.getStr("business_ids");
            List<CrmBusiness> businessList = new ArrayList<>();
            if (businessIds != null) {
                String[] businessIdsArr = businessIds.split(",");
                for (String businessId : businessIdsArr) {
                    businessList.add(CrmBusiness.dao.findById(Integer.valueOf(businessId)));
                }
            }
            String contactsIds = record.getStr("contacts_ids");
            List<CrmContacts> contactsList = new ArrayList<>();
            if (contactsIds != null) {
                String[] contactsIdsArr = contactsIds.split(",");
                for (String contactsId : contactsIdsArr) {
                    contactsList.add(CrmContacts.dao.findById(Integer.valueOf(contactsId)));
                }
            }
            record.set("business_list", businessList).set("contacts_list", contactsList);
        });
        return recordList;
    }

    //提交审核
    @Before(Tx.class)
    public R auditCase(Integer caseId) {
        Map<String, Integer> map = new HashMap<>();
        //创建审核记录
        AdminExamineRecord examineRecord = new AdminExamineRecord();
        examineRecord.setCreateTime(DateUtil.date());
        examineRecord.setCreateUser(BaseUtil.getUser().getUserId());
        CrmCase crmCase = CrmCase.dao.findById(caseId);

        //创建审核日志
        AdminExamineLog examineLog = new AdminExamineLog();
        examineRecord.setExamineStatus(3);
        examineLog.setCreateTime(DateUtil.date());
        examineLog.setCreateUser(BaseUtil.getUser().getUserId());
        examineLog.setExamineStatus(0);//未审核
        examineLog.setOrderId(1);
        //根据模板id查询当前审批流程
        AdminExamine examine = AdminExamine.dao.findFirst(Db.getSql("admin.examine.getExamineById"), crmCase.getExamineId());
        if (examine == null) {
            map.put("status", 0);
        } else {
            examineRecord.setExamineId(examine.getExamineId());
            //固定审批
            //先查询该审批流程的审批步骤的第一步
            AdminExamineStep examineStep = AdminExamineStep.dao.findFirst(Db.getSql("admin.examineStep.queryExamineStepByExamineIdOrderByStepNum"), examine.getExamineId());
            examineRecord.setExamineStepId(examineStep.getStepId());//当前进行审批的步骤
            examineLog.setExamineStepId(examineStep.getStepId());
            examineRecord.save();
            crmCase.setExamineRecordId(examineRecord.getRecordId());
            //判断步骤类型 指定用户
            if (examineStep.getStepType() == 2 || examineStep.getStepType() == 3) {
                String[] userIds = examineStep.getCheckUserId().split(",");
                for (String id : userIds) {
                    if (StrUtil.isNotEmpty(id)) {
                        examineLog.setLogId(null);
                        examineLog.setExamineUser(Long.valueOf(id));
                        examineLog.setRecordId(examineRecord.getRecordId());
                        examineLog.setIsRecheck(0);
                        examineLog.save();
                    }
                }
            } else if (examineStep.getStepType() == 1) {
                //如果是负责人主管审批 获取主管ID
                Record r = Db.findFirst(Db.getSql("admin.examineLog.queryUserByUserId"), crmCase.getOwnerUserId());
                if (r == null || r.getLong("user_id") == null) {
                    examineLog.setExamineUser(BaseConstant.SUPER_ADMIN_USER_ID);
                } else {
                    examineLog.setExamineUser(r.getLong("user_id"));
                }
                examineLog.setRecordId(examineRecord.getRecordId());
                examineLog.setIsRecheck(0);
                examineLog.save();
            } else {
                //如果是负责人主管审批 获取主管的主管ID
                Record r = Db.findFirst(Db.getSql("admin.examineLog.queryUserByUserId"), Db.findFirst(Db.getSql("admin.examineLog.queryUserByUserId"), crmCase.getOwnerUserId()).getLong("user_id"));
                if (r == null || r.getLong("user_id") == null) {
                    examineLog.setExamineUser(Long.valueOf(BaseConstant.SUPER_ADMIN_USER_ID));
                } else {
                    examineLog.setExamineUser(r.getLong("user_id"));
                }
                examineLog.setRecordId(examineRecord.getRecordId());
                examineLog.setIsRecheck(0);
                examineLog.save();
            }
            crmCase.setExamineId(examineRecord.getExamineId());
            crmCase.setCheckStatus(1);
            crmCase.setUpdateTime(DateUtil.date());
            crmCase.update();
        }
        return R.ok();
    }
}
